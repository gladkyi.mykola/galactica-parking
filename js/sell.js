document.addEventListener('DOMContentLoaded', function () {
    var iconsContainer = document.getElementById('icons-container');
    var sell = document.getElementById('sell');
    var priceSell = document.getElementById('priceSell');
    var contactsSell = document.getElementById('contactsSell')

    var iconCoordinates = [
//
// 1 поверх - верх
// 1.01 
//         {top: 40, left: 455, info: {priceSell: '99999', contactsSell: '123456789'}},
// 1.02
//         {top: 40, left: 505, info: {priceSell: '7000', contactsSell: ''}},
// 1.03 
//         {top: 40, left: 545, info: {priceSell: '2000', contactsSell: ''}},
// 1.04 
//         {top: 40, left: 585, info: {priceSell: '', contactsSell: ''}},
// 1.05 
//         {top: 40, left: 625, info: {priceSell: ' грн', contactsSell: ''}},
// 1.06 
//         {top: 40, left: 665, info: {priceSell: ' грн', contactsSell: ''}},
// 1.07 
//         {top: 40, left: 705, info: {priceSell: ' грн', contactsSell: ''}},
// 1.08 
//         {top: 40, left: 745, info: {priceSell: ' грн', contactsSell: ''}},
// 1.09 
//         {top: 40, left: 785, info: {priceSell: ' грн', contactsSell: ''}},
// 1.10 
//         {top: 40, left: 825, info: {priceSell: ' грн', contactsSell: ''}},
// 1.11 
//         {top: 40, left: 865, info: {priceSell: ' грн', contactsSell: ''}},
// 1.12 
//         {top: 40, left: 905, info: {priceSell: ' грн', contactsSell: ''}},
// 1.13 
//         {top: 40, left: 945, info: {priceSell: ' грн', contactsSell: ''}},
// 1.14 
//         {top: 40, left: 985, info: {priceSell: ' грн', contactsSell: ''}},
// 1.15 
//         {top: 40, left: 1025, info: {priceSell: ' грн', contactsSell: ''}},
// 1.16 
//         {top: 40, left: 1065, info: {priceSell: ' грн', contactsSell: ''}},
// 1.17 
//         {top: 40, left: 1105, info: {priceSell: ' грн', contactsSell: ''}},
// 1.18 
//         {top: 40, left: 1145, info: {priceSell: ' грн', contactsSell: ''}},
// 1.19 
        {top: 40, left: 1185, info: {priceSell: '$21000', contactsSell: '098-897-0909'}},
// 1.20 
//         {top: 40, left: 1225, info: {priceSell: ' грн', contactsSell: ''}},
// 1.21 
//         {top: 40, left: 1265, info: {priceSell: ' грн', contactsSell: ''}},
// 1.22 
//         {top: 40, left: 1305, info: {priceSell: ' грн', contactsSell: ''}},
// 1.23 
//         {top: 40, left: 1345, info: {priceSell: ' грн', contactsSell: ''}},
// 1.24 
//         {top: 40, left: 1385, info: {priceSell: ' грн', contactsSell: ''}},
// 1.25 
//         {top: 40, left: 1425, info: {priceSell: ' грн', contactsSell: ''}},
// 1.26 
//         {top: 40, left: 1465, info: {priceSell: ' грн', contactsSell: ''}},
// 1.27 
//         {top: 40, left: 1505, info: {priceSell: ' грн', contactsSell: ''}},
// 1.28 
//         {top: 40, left: 1545, info: {priceSell: ' грн', contactsSell: ''}},
// 1.29 
//         {top: 40, left: 1585, info: {priceSell: ' грн', contactsSell: ''}},
// 1.30 
//         {top: 40, left: 1625, info: {priceSell: ' грн', contactsSell: ''}},
// 1.31 
//         {top: 40, left: 1665, info: {priceSell: ' грн', contactsSell: ''}},
// 1.32 
//         {top: 40, left: 1705, info: {priceSell: ' грн', contactsSell: ''}},
// 1.33 
//         {top: 40, left: 1745, info: {priceSell: ' грн', contactsSell: ''}},
// 1.34 
//         {top: 40, left: 1785, info: {priceSell: ' грн', contactsSell: ''}},
// 1.35 
//         {top: 40, left: 1825, info: {priceSell: ' грн', contactsSell: ''}},
// 1.36 
//         {top: 40, left: 1865, info: {priceSell: ' грн', contactsSell: ''}},
// 1.37 
//         {top: 40, left: 1905, info: {priceSell: ' грн', contactsSell: ''}},
// 1.38 
//         {top: 40, left: 1955, info: {priceSell: ' грн', contactsSell: ''}},
//
// 1 поверх - низ
// 1.78
//         {top: 190, left: 265, info: {priceSell: ' грн', contactsSell: ''}},
// 1.77
//         {top: 190, left: 305, info: {priceSell: ' грн', contactsSell: ''}},
// 1.76
//         {top: 190, left: 345, info: {priceSell: ' грн', contactsSell: ''}},
// 1.75
//         {top: 190, left: 385, info: {priceSell: ' грн', contactsSell: ''}},
// 1.74
//         {top: 190, left: 425, info: {priceSell: ' грн', contactsSell: ''}},
// 1.73
//         {top: 190, left: 465, info: {priceSell: ' грн', contactsSell: ''}},
// 1.72
//         {top: 190, left: 505, info: {priceSell: ' грн', contactsSell: ''}},
// 1.71
//         {top: 190, left: 545, info: {priceSell: ' грн', contactsSell: ''}},
// 1.70
//         {top: 190, left: 585, info: {priceSell: ' грн', contactsSell: ''}},
// 1.69
//         {top: 190, left: 635, info: {priceSell: ' грн', contactsSell: ''}},
// 1.68
//         {top: 190, left: 855, info: {priceSell: ' грн', contactsSell: ''}},
// 1.67
//         {top: 190, left: 905, info: {priceSell: ' грн', contactsSell: ''}},
// 1.66
//         {top: 190, left: 945, info: {priceSell: ' грн', contactsSell: ''}},
// 1.65
//         {top: 190, left: 985, info: {priceSell: ' грн', contactsSell: ''}},
// 1.64
//         {top: 190, left: 1025, info: {priceSell: ' грн', contactsSell: ''}},
// 1.63
//         {top: 190, left: 1065, info: {priceSell: ' грн', contactsSell: ''}},
// 1.62
//         {top: 190, left: 1105, info: {priceSell: ' грн', contactsSell: ''}},
// 1.61
//         {top: 190, left: 1145, info: {priceSell: ' грн', contactsSell: ''}},
// 1.60
//         {top: 190, left: 1185, info: {priceSell: ' грн', contactsSell: ''}},
// 1.59
//         {top: 190, left: 1225, info: {priceSell: ' грн', contactsSell: ''}},
// 1.58
//         {top: 190, left: 1265, info: {priceSell: ' грн', contactsSell: ''}},
// 1.57
//         {top: 190, left: 1305, info: {priceSell: ' грн', contactsSell: ''}},
// 1.56
//         {top: 190, left: 1345, info: {priceSell: ' грн', contactsSell: ''}},
// 1.55
//         {top: 190, left: 1385, info: {priceSell: ' грн', contactsSell: ''}},
// 1.54
//         {top: 190, left: 1425, info: {priceSell: ' грн', contactsSell: ''}},
// 1.53
//         {top: 190, left: 1465, info: {priceSell: ' грн', contactsSell: ''}},
// 1.52
//         {top: 190, left: 1505, info: {priceSell: ' грн', contactsSell: ''}},
// 1.51
//         {top: 190, left: 1545, info: {priceSell: ' грн', contactsSell: ''}},
// 1.50
//         {top: 190, left: 1585, info: {priceSell: ' грн', contactsSell: ''}},
// 1.49
//         {top: 190, left: 1625, info: {priceSell: ' грн', contactsSell: ''}},
// 1.48
//         {top: 190, left: 1780, info: {priceSell: ' грн', contactsSell: ''}},
// 1.47
//         {top: 190, left: 1825, info: {priceSell: ' грн', contactsSell: ''}},
// 1.46
//         {top: 190, left: 1865, info: {priceSell: ' грн', contactsSell: ''}},
// 1.45
//         {top: 190, left: 1905, info: {priceSell: ' грн', contactsSell: ''}},
// 1.44
//         {top: 190, left: 1945, info: {priceSell: ' грн', contactsSell: ''}},
// 1.43
//         {top: 190, left: 1985, info: {priceSell: ' грн', contactsSell: ''}},
// 1.42
//         {top: 190, left: 2025, info: {priceSell: ' грн', contactsSell: ''}},
// 1.41
//         {top: 190, left: 2065, info: {priceSell: ' грн', contactsSell: ''}},
// 1.40
//         {top: 190, left: 2105, info: {priceSell: ' грн', contactsSell: ''}},
// 1.39
//         {top: 190, left: 2145, info: {priceSell: ' грн', contactsSell: ''}},
//
//
// 2 поверх - верх
// 2.01
//         {top: 415, left: 265, info: {priceSell: ' грн', contactsSell: ''}},
// 2.02
//         {top: 415, left: 305, info: {priceSell: ' грн', contactsSell: ''}},
// 2.03
//         {top: 415, left: 345, info: {priceSell: ' грн', contactsSell: ''}},
// 2.04
//         {top: 415, left: 385, info: {priceSell: ' грн', contactsSell: ''}},
// 2.05
//         {top: 415, left: 425, info: {priceSell: ' грн', contactsSell: ''}},
// 2.06
//         {top: 415, left: 465, info: {priceSell: ' грн', contactsSell: ''}},
// 2.07
//         {top: 415, left: 505, info: {priceSell: ' грн', contactsSell: ''}},
// 2.08
//         {top: 415, left: 545, info: {priceSell: ' грн', contactsSell: ''}},
// 2.09
//         {top: 415, left: 585, info: {priceSell: ' грн', contactsSell: ''}},
// 2.10
//         {top: 415, left: 625, info: {priceSell: ' грн', contactsSell: ''}},
// 2.11
//         {top: 415, left: 665, info: {priceSell: ' грн', contactsSell: ''}},
// 2.12
//         {top: 415, left: 705, info: {priceSell: ' грн', contactsSell: ''}},
// 2.13
//         {top: 415, left: 745, info: {priceSell: ' грн', contactsSell: ''}},
// 2.14
//         {top: 415, left: 785, info: {priceSell: ' грн', contactsSell: ''}},
// 2.15
//         {top: 415, left: 825, info: {priceSell: ' грн', contactsSell: ''}},
// 2.16
//         {top: 415, left: 865, info: {priceSell: ' грн', contactsSell: ''}},
// 2.17
//         {top: 415, left: 905, info: {priceSell: ' грн', contactsSell: ''}},
// 2.18
//         {top: 415, left: 945, info: {priceSell: ' грн', contactsSell: ''}},
// 2.19
        {top: 415, left: 985, info: {priceSell: '$16000', contactsSell: '0675041496'}},
// 2.20
//         {top: 415, left: 1025, info: {priceSell: ' грн', contactsSell: ''}},
// 2.21
//         {top: 415, left: 1065, info: {priceSell: ' грн', contactsSell: ''}},
// 2.22
//         {top: 415, left: 1105, info: {priceSell: ' грн', contactsSell: ''}},
// 2.23
//         {top: 415, left: 1145, info: {priceSell: ' грн', contactsSell: ''}},
// 2.24
//         {top: 415, left: 1185, info: {priceSell: ' грн', contactsSell: ''}},
// 2.25
//         {top: 415, left: 1225, info: {priceSell: ' грн', contactsSell: ''}},
// 2.26
//         {top: 415, left: 1265, info: {priceSell: ' грн', contactsSell: ''}},
// 2.27
//         {top: 415, left: 1305, info: {priceSell: ' грн', contactsSell: ''}},
// 2.28
//         {top: 415, left: 1345, info: {priceSell: ' грн', contactsSell: ''}},
// 2.29
//         {top: 415, left: 1385, info: {priceSell: ' грн', contactsSell: ''}},
// 2.30
//         {top: 415, left: 1425, info: {priceSell: ' грн', contactsSell: ''}},
// 2.31
//         {top: 415, left: 1465, info: {priceSell: ' грн', contactsSell: ''}},
// 2.32
//         {top: 415, left: 1505, info: {priceSell: ' грн', contactsSell: ''}},
// 2.33
//         {top: 415, left: 1545, info: {priceSell: ' грн', contactsSell: ''}},
// 2.34
//         {top: 415, left: 1585, info: {priceSell: ' грн', contactsSell: ''}},
// 2.35
//         {top: 415, left: 1625, info: {priceSell: ' грн', contactsSell: ''}},
// 2.36
//         {top: 415, left: 1665, info: {priceSell: ' грн', contactsSell: ''}},
// 2.37
//         {top: 415, left: 1705, info: {priceSell: ' грн', contactsSell: ''}},
// 2.38
//         {top: 415, left: 1745, info: {priceSell: ' грн', contactsSell: ''}},
// 2.39
//         {top: 415, left: 1785, info: {priceSell: ' грн', contactsSell: ''}},
// 2.40
//         {top: 415, left: 1825, info: {priceSell: ' грн', contactsSell: ''}},
// 2.41
//         {top: 415, left: 1865, info: {priceSell: ' грн', contactsSell: ''}},
// 2.42
//         {top: 415, left: 1905, info: {priceSell: ' грн', contactsSell: ''}},
// 2.43
//         {top: 415, left: 1945, info: {priceSell: ' грн', contactsSell: ''}},
// 2.44
//         {top: 415, left: 1985, info: {priceSell: ' грн', contactsSell: ''}},
// 2.45
//         {top: 415, left: 2025, info: {priceSell: ' грн', contactsSell: ''}},
// 2.46
//         {top: 415, left: 2065, info: {priceSell: ' грн', contactsSell: ''}},
// 2.47
//         {top: 415, left: 2105, info: {priceSell: ' грн', contactsSell: ''}},
// 2.48
//         {top: 415, left: 2145, info: {priceSell: ' грн', contactsSell: ''}},
//
//
// 2 поверх - низ
// 2.92
//         {top: 565, left: 265, info: {priceSell: ' грн', contactsSell: ''}},
// 2.91
//         {top: 565, left: 305, info: {priceSell: ' грн', contactsSell: ''}},
// 2.90
//         {top: 565, left: 345, info: {priceSell: ' грн', contactsSell: ''}},
// 2.89
//         {top: 565, left: 385, info: {priceSell: ' грн', contactsSell: ''}},
// 2.88
//         {top: 565, left: 425, info: {priceSell: ' грн', contactsSell: ''}},
// 2.87
//         {top: 565, left: 465, info: {priceSell: ' грн', contactsSell: ''}},
// 2.86
//         {top: 565, left: 505, info: {priceSell: ' грн', contactsSell: ''}},
// 2.85
//         {top: 565, left: 545, info: {priceSell: ' грн', contactsSell: ''}},
// 2.84
//         {top: 565, left: 585, info: {priceSell: ' грн', contactsSell: ''}},
// 2.83
//         {top: 565, left: 625, info: {priceSell: ' грн', contactsSell: ''}},
// 2.82
//         {top: 565, left: 665, info: {priceSell: ' грн', contactsSell: ''}},
// 2.81
//         {top: 565, left: 785, info: {priceSell: ' грн', contactsSell: ''}},
// 2.80
//         {top: 565, left: 825, info: {priceSell: ' грн', contactsSell: ''}},
// 2.79
//         {top: 565, left: 865, info: {priceSell: ' грн', contactsSell: ''}},
// 2.78
//         {top: 565, left: 905, info: {priceSell: ' грн', contactsSell: ''}},
// 2.77
//         {top: 565, left: 945, info: {priceSell: ' грн', contactsSell: ''}},
// 2.76
//         {top: 565, left: 985, info: {priceSell: ' грн', contactsSell: ''}},
// 2.75
//         {top: 565, left: 1025, info: {priceSell: ' грн', contactsSell: ''}},
// 2.74
//         {top: 565, left: 1065, info: {priceSell: ' грн', contactsSell: ''}},
// 2.73
//         {top: 565, left: 1105, info: {priceSell: ' грн', contactsSell: ''}},
// 2.72
//         {top: 565, left: 1145, info: {priceSell: ' грн', contactsSell: ''}},
// 2.71
//         {top: 565, left: 1185, info: {priceSell: ' грн', contactsSell: ''}},
// 2.70
//         {top: 565, left: 1225, info: {priceSell: ' грн', contactsSell: ''}},
// 2.69
//         {top: 565, left: 1265, info: {priceSell: ' грн', contactsSell: ''}},
// 2.68
//         {top: 565, left: 1305, info: {priceSell: ' грн', contactsSell: ''}},
// 2.67
//         {top: 565, left: 1345, info: {priceSell: ' грн', contactsSell: ''}},
// 2.66
//         {top: 565, left: 1385, info: {priceSell: ' грн', contactsSell: ''}},
// 2.65
//         {top: 565, left: 1425, info: {priceSell: ' грн', contactsSell: ''}},
// 2.64
//         {top: 565, left: 1465, info: {priceSell: ' грн', contactsSell: ''}},
// 2.63
//         {top: 565, left: 1505, info: {priceSell: ' грн', contactsSell: ''}},
// 2.62
//         {top: 565, left: 1545, info: {priceSell: ' грн', contactsSell: ''}},
// 2.61
//         {top: 565, left: 1585, info: {priceSell: ' грн', contactsSell: ''}},
// 2.60
//         {top: 565, left: 1625, info: {priceSell: ' грн', contactsSell: ''}},
// 2.59
        {top: 565, left: 1745, info: {priceSell: '$20000', contactsSell: '0675041496'}},
// 2.58
//         {top: 565, left: 1785, info: {priceSell: ' грн', contactsSell: ''}},
// 2.57
//         {top: 565, left: 1825, info: {priceSell: ' грн', contactsSell: ''}},
// 2.56
//         {top: 565, left: 1865, info: {priceSell: ' грн', contactsSell: ''}},
// 2.55
//         {top: 565, left: 1905, info: {priceSell: ' грн', contactsSell: ''}},
// 2.54
//         {top: 565, left: 1945, info: {priceSell: ' грн', contactsSell: ''}},
// 2.53
//         {top: 565, left: 1985, info: {priceSell: ' грн', contactsSell: ''}},
// 2.52
//         {top: 565, left: 2025, info: {priceSell: ' грн', contactsSell: ''}},
// 2.51
//         {top: 565, left: 2065, info: {priceSell: ' грн', contactsSell: ''}},
// 2.50
//         {top: 565, left: 2105, info: {priceSell: ' грн', contactsSell: ''}},
// 2.49
//         {top: 565, left: 2145, info: {priceSell: ' грн', contactsSell: ''}},
//
//
// 3 поверх - верх
// 3.01
//         {top: 815, left: 265, info: {priceSell: ' грн', contactsSell: ''}},
// 3.02
//         {top: 815, left: 305, info: {priceSell: ' грн', contactsSell: ''}},
// 3.03
//         {top: 815, left: 345, info: {priceSell: ' грн', contactsSell: ''}},
// 3.04
//         {top: 815, left: 385, info: {priceSell: ' грн', contactsSell: ''}},
// 3.05
//         {top: 815, left: 425, info: {priceSell: ' грн', contactsSell: ''}},
// 3.06
//         {top: 815, left: 465, info: {priceSell: ' грн', contactsSell: ''}},
// 3.07
//         {top: 815, left: 505, info: {priceSell: ' грн', contactsSell: ''}},
// 3.08
//         {top: 815, left: 545, info: {priceSell: ' грн', contactsSell: ''}},
// 3.09
//         {top: 815, left: 585, info: {priceSell: ' грн', contactsSell: ''}},
// 3.10
//         {top: 815, left: 625, info: {priceSell: ' грн', contactsSell: ''}},
// 3.11
//         {top: 815, left: 665, info: {priceSell: ' грн', contactsSell: ''}},
// 3.12
//         {top: 815, left: 705, info: {priceSell: ' грн', contactsSell: ''}},
// 3.13
//         {top: 815, left: 745, info: {priceSell: ' грн', contactsSell: ''}},
// 3.14
//         {top: 815, left: 785, info: {priceSell: ' грн', contactsSell: ''}},
// 3.15
//         {top: 815, left: 825, info: {priceSell: ' грн', contactsSell: ''}},
// 3.16
//         {top: 815, left: 865, info: {priceSell: ' грн', contactsSell: ''}},
// 3.17
//         {top: 815, left: 905, info: {priceSell: ' грн', contactsSell: ''}},
// 3.18
//         {top: 815, left: 945, info: {priceSell: ' грн', contactsSell: ''}},
// 3.19
//         {top: 815, left: 985, info: {priceSell: ' грн', contactsSell: ''}},
// 3.20
//         {top: 815, left: 1025, info: {priceSell: ' грн', contactsSell: ''}},
// 3.21
//         {top: 815, left: 1065, info: {priceSell: ' грн', contactsSell: ''}},
// 3.22
//         {top: 815, left: 1105, info: {priceSell: ' грн', contactsSell: ''}},
// 3.23
//         {top: 815, left: 1145, info: {priceSell: ' грн', contactsSell: ''}},
// 3.24
//         {top: 815, left: 1185, info: {priceSell: ' грн', contactsSell: ''}},
// 3.25
//         {top: 815, left: 1225, info: {priceSell: ' грн', contactsSell: ''}},
// 3.26
//         {top: 815, left: 1265, info: {priceSell: ' грн', contactsSell: ''}},
// 3.27
//         {top: 815, left: 1305, info: {priceSell: ' грн', contactsSell: ''}},
// 3.28
//         {top: 815, left: 1345, info: {priceSell: ' грн', contactsSell: ''}},
// 3.29
//         {top: 815, left: 1385, info: {priceSell: ' грн', contactsSell: ''}},
// 3.30
//         {top: 815, left: 1425, info: {priceSell: ' грн', contactsSell: ''}},
// 3.31
//         {top: 815, left: 1465, info: {priceSell: ' грн', contactsSell: ''}},
// 3.32
//         {top: 815, left: 1505, info: {priceSell: ' грн', contactsSell: ''}},
// 3.33
//         {top: 815, left: 1545, info: {priceSell: ' грн', contactsSell: ''}},
// 3.34
//         {top: 815, left: 1585, info: {priceSell: ' грн', contactsSell: ''}},
// 3.35
//         {top: 815, left: 1625, info: {priceSell: ' грн', contactsSell: ''}},
// 3.36
//         {top: 815, left: 1665, info: {priceSell: ' грн', contactsSell: ''}},
// 3.37
//         {top: 815, left: 1705, info: {priceSell: ' грн', contactsSell: ''}},
// 3.38
//         {top: 815, left: 1745, info: {priceSell: ' грн', contactsSell: ''}},
// 3.39
//         {top: 815, left: 1785, info: {priceSell: ' грн', contactsSell: ''}},
// 3.40
//         {top: 815, left: 1825, info: {priceSell: ' грн', contactsSell: ''}},
// 3.41
//         {top: 815, left: 1865, info: {priceSell: ' грн', contactsSell: ''}},
// 3.42
//         {top: 815, left: 1905, info: {priceSell: ' грн', contactsSell: ''}},
// 3.43
//         {top: 815, left: 1945, info: {priceSell: ' грн', contactsSell: ''}},
// 3.44
//         {top: 815, left: 1985, info: {priceSell: ' грн', contactsSell: ''}},
// 3.45
//         {top: 815, left: 2025, info: {priceSell: ' грн', contactsSell: ''}},
// 3.46
//         {top: 815, left: 2065, info: {priceSell: ' грн', contactsSell: ''}},
// 3.47
//         {top: 815, left: 2105, info: {priceSell: ' грн', contactsSell: ''}},
// 3.48
//         {top: 815, left: 2145, info: {priceSell: ' грн', contactsSell: ''}},
//
//
// 3 поверх - низ
// 3.92
//         {top: 965, left: 265, info: {priceSell: ' грн', contactsSell: ''}},
// 3.91
//         {top: 965, left: 305, info: {priceSell: ' грн', contactsSell: ''}},
// 3.90
//         {top: 965, left: 345, info: {priceSell: ' грн', contactsSell: ''}},
// 3.89
//         {top: 965, left: 385, info: {priceSell: ' грн', contactsSell: ''}},
// 3.88
//         {top: 965, left: 425, info: {priceSell: ' грн', contactsSell: ''}},
// 3.87
//         {top: 965, left: 465, info: {priceSell: ' грн', contactsSell: ''}},
// 3.86
//         {top: 965, left: 505, info: {priceSell: ' грн', contactsSell: ''}},
// 3.85
//         {top: 965, left: 545, info: {priceSell: ' грн', contactsSell: ''}},
// 3.84
//         {top: 965, left: 585, info: {priceSell: ' грн', contactsSell: ''}},
// 3.83
//         {top: 965, left: 625, info: {priceSell: ' грн', contactsSell: ''}},
// 3.82
//         {top: 965, left: 665, info: {priceSell: ' грн', contactsSell: ''}},
// 3.81
//         {top: 965, left: 785, info: {priceSell: ' грн', contactsSell: ''}},
// 3.80
//         {top: 965, left: 825, info: {priceSell: ' грн', contactsSell: ''}},
// 3.79
//         {top: 965, left: 865, info: {priceSell: ' грн', contactsSell: ''}},
// 3.78
//         {top: 965, left: 905, info: {priceSell: ' грн', contactsSell: ''}},
// 3.77
//         {top: 965, left: 945, info: {priceSell: ' грн', contactsSell: ''}},
// 3.76
//         {top: 965, left: 985, info: {priceSell: ' грн', contactsSell: ''}},
// 3.75
//         {top: 965, left: 1025, info: {priceSell: ' грн', contactsSell: ''}},
// 3.74
//         {top: 965, left: 1065, info: {priceSell: ' грн', contactsSell: ''}},
// 3.73
//         {top: 965, left: 1105, info: {priceSell: ' грн', contactsSell: ''}},
// 3.72
//         {top: 965, left: 1145, info: {priceSell: ' грн', contactsSell: ''}},
// 3.71
//         {top: 965, left: 1185, info: {priceSell: ' грн', contactsSell: ''}},
// 3.70
//         {top: 965, left: 1225, info: {priceSell: ' грн', contactsSell: ''}},
// 3.69
//         {top: 965, left: 1265, info: {priceSell: ' грн', contactsSell: ''}},
// 3.68
//         {top: 965, left: 1305, info: {priceSell: ' грн', contactsSell: ''}},
// 3.67
//         {top: 965, left: 1345, info: {priceSell: ' грн', contactsSell: ''}},
// 3.66
//         {top: 965, left: 1385, info: {priceSell: ' грн', contactsSell: ''}},
// 3.65
//         {top: 965, left: 1425, info: {priceSell: ' грн', contactsSell: ''}},
// 3.64
//         {top: 965, left: 1465, info: {priceSell: ' грн', contactsSell: ''}},
// 3.63
//         {top: 965, left: 1505, info: {priceSell: ' грн', contactsSell: ''}},
// 3.62
//         {top: 965, left: 1545, info: {priceSell: ' грн', contactsSell: ''}},
// 3.61
//         {top: 965, left: 1585, info: {priceSell: ' грн', contactsSell: ''}},
// 3.60
//         {top: 965, left: 1625, info: {priceSell: ' грн', contactsSell: ''}},
// 3.59
//         {top: 965, left: 1745, info: {priceSell: ' грн', contactsSell: ''}},
// 3.58
//         {top: 965, left: 1785, info: {priceSell: ' грн', contactsSell: ''}},
// 3.57
//         {top: 965, left: 1825, info: {priceSell: ' грн', contactsSell: ''}},
// 3.56
//         {top: 965, left: 1865, info: {priceSell: ' грн', contactsSell: ''}},
// 3.55
//         {top: 965, left: 1905, info: {priceSell: ' грн', contactsSell: ''}},
// 3.54
//         {top: 965, left: 1945, info: {priceSell: ' грн', contactsSell: ''}},
// 3.53
//         {top: 965, left: 1985, info: {priceSell: ' грн', contactsSell: ''}},
// 3.52
//         {top: 965, left: 2025, info: {priceSell: ' грн', contactsSell: ''}},
// 3.51
//         {top: 965, left: 2065, info: {priceSell: ' грн', contactsSell: ''}},
// 3.50
//         {top: 965, left: 2105, info: {priceSell: ' грн', contactsSell: ''}},
// 3.49
//         {top: 965, left: 2145, info: {priceSell: ' грн', contactsSell: ''}},
//
//
// 4 поверх - верх
// 4.01
//         {top: 1215, left: 265, info: {priceSell: ' грн', contactsSell: ''}},
// 4.02
//         {top: 1215, left: 305, info: {priceSell: ' грн', contactsSell: ''}},
// 4.03
//         {top: 1215, left: 345, info: {priceSell: ' грн', contactsSell: ''}},
// 4.04
//         {top: 1215, left: 385, info: {priceSell: ' грн', contactsSell: ''}},
// 4.05
//         {top: 1215, left: 425, info: {priceSell: ' грн', contactsSell: ''}},
// 4.06
//         {top: 1215, left: 465, info: {priceSell: ' грн', contactsSell: ''}},
// 4.07
//         {top: 1215, left: 505, info: {priceSell: ' грн', contactsSell: ''}},
// 4.08
//         {top: 1215, left: 545, info: {priceSell: ' грн', contactsSell: ''}},
// 4.09
//         {top: 1215, left: 585, info: {priceSell: ' грн', contactsSell: ''}},
// 4.10
//         {top: 1215, left: 625, info: {priceSell: ' грн', contactsSell: ''}},
// 4.11
//         {top: 1215, left: 665, info: {priceSell: ' грн', contactsSell: ''}},
// 4.12
//         {top: 1215, left: 705, info: {priceSell: ' грн', contactsSell: ''}},
// 4.13
//         {top: 1215, left: 745, info: {priceSell: ' грн', contactsSell: ''}},
// 4.14
//         {top: 1215, left: 785, info: {priceSell: ' грн', contactsSell: ''}},
// 4.15
//         {top: 1215, left: 825, info: {priceSell: ' грн', contactsSell: ''}},
// 4.16
//         {top: 1215, left: 865, info: {priceSell: ' грн', contactsSell: ''}},
// 4.17
//         {top: 1215, left: 905, info: {priceSell: ' грн', contactsSell: ''}},
// 4.18
//         {top: 1215, left: 945, info: {priceSell: ' грн', contactsSell: ''}},
// 4.19
//         {top: 1215, left: 985, info: {priceSell: ' грн', contactsSell: ''}},
// 4.20
//         {top: 1215, left: 1025, info: {priceSell: ' грн', contactsSell: ''}},
// 4.21
//         {top: 1215, left: 1065, info: {priceSell: ' грн', contactsSell: ''}},
// 4.22
//         {top: 1215, left: 1105, info: {priceSell: ' грн', contactsSell: ''}},
// 4.23
//         {top: 1215, left: 1145, info: {priceSell: ' грн', contactsSell: ''}},
// 4.24
//         {top: 1215, left: 1185, info: {priceSell: ' грн', contactsSell: ''}},
// 4.25
//         {top: 1215, left: 1225, info: {priceSell: ' грн', contactsSell: ''}},
// 4.26
//         {top: 1215, left: 1265, info: {priceSell: ' грн', contactsSell: ''}},
// 4.27
//         {top: 1215, left: 1305, info: {priceSell: ' грн', contactsSell: ''}},
// 4.28
//         {top: 1215, left: 1345, info: {priceSell: ' грн', contactsSell: ''}},
// 4.29
//         {top: 1215, left: 1385, info: {priceSell: ' грн', contactsSell: ''}},
// 4.30
//         {top: 1215, left: 1425, info: {priceSell: ' грн', contactsSell: ''}},
// 4.31
//         {top: 1215, left: 1465, info: {priceSell: ' грн', contactsSell: ''}},
// 4.32
//         {top: 1215, left: 1505, info: {priceSell: ' грн', contactsSell: ''}},
// 4.33
//         {top: 1215, left: 1545, info: {priceSell: ' грн', contactsSell: ''}},
// 4.34
//         {top: 1215, left: 1585, info: {priceSell: ' грн', contactsSell: ''}},
// 4.35
//         {top: 1215, left: 1625, info: {priceSell: ' грн', contactsSell: ''}},
// 4.36
//         {top: 1215, left: 1665, info: {priceSell: ' грн', contactsSell: ''}},
// 4.37
//         {top: 1215, left: 1705, info: {priceSell: ' грн', contactsSell: ''}},
// 4.38
//         {top: 1215, left: 1745, info: {priceSell: ' грн', contactsSell: ''}},
// 4.39
//         {top: 1215, left: 1785, info: {priceSell: ' грн', contactsSell: ''}},
// 4.40
//         {top: 1215, left: 1825, info: {priceSell: ' грн', contactsSell: ''}},
// 4.41
//         {top: 1215, left: 1865, info: {priceSell: ' грн', contactsSell: ''}},
// 4.42
//         {top: 1215, left: 1905, info: {priceSell: ' грн', contactsSell: ''}},
// 4.43
//         {top: 1215, left: 1945, info: {priceSell: ' грн', contactsSell: ''}},
// 4.44
//         {top: 1215, left: 1985, info: {priceSell: ' грн', contactsSell: ''}},
// 4.45
//         {top: 1215, left: 2025, info: {priceSell: ' грн', contactsSell: ''}},
// 4.46
//         {top: 1215, left: 2065, info: {priceSell: ' грн', contactsSell: ''}},
// 4.47
//         {top: 1215, left: 2105, info: {priceSell: ' грн', contactsSell: ''}},
// 4.48
//         {top: 1215, left: 2145, info: {priceSell: ' грн', contactsSell: ''}},
//
//
// 4 поверх - низ
// 4.92
//         {top: 1365, left: 265, info: {priceSell: ' грн', contactsSell: ''}},
// 4.91
//         {top: 1365, left: 305, info: {priceSell: ' грн', contactsSell: ''}},
// 4.90
//         {top: 1365, left: 345, info: {priceSell: ' грн', contactsSell: ''}},
// 4.89
//         {top: 1365, left: 385, info: {priceSell: ' грн', contactsSell: ''}},
// 4.88
//         {top: 1365, left: 425, info: {priceSell: ' грн', contactsSell: ''}},
// 4.87
//         {top: 1365, left: 465, info: {priceSell: ' грн', contactsSell: ''}},
// 4.86
//         {top: 1365, left: 505, info: {priceSell: ' грн', contactsSell: ''}},
// 4.85
//         {top: 1365, left: 545, info: {priceSell: ' грн', contactsSell: ''}},
// 4.84
//         {top: 1365, left: 585, info: {priceSell: ' грн', contactsSell: ''}},
// 4.83
//         {top: 1365, left: 625, info: {priceSell: ' грн', contactsSell: ''}},
// 4.82
//         {top: 1365, left: 665, info: {priceSell: ' грн', contactsSell: ''}},
// 4.81
//         {top: 1365, left: 785, info: {priceSell: ' грн', contactsSell: ''}},
// 4.80
//         {top: 1365, left: 825, info: {priceSell: ' грн', contactsSell: ''}},
// 4.79
//         {top: 1365, left: 865, info: {priceSell: ' грн', contactsSell: ''}},
// 4.78
//         {top: 1365, left: 905, info: {priceSell: ' грн', contactsSell: ''}},
// 4.77
//         {top: 1365, left: 945, info: {priceSell: ' грн', contactsSell: ''}},
// 4.76
//         {top: 1365, left: 985, info: {priceSell: ' грн', contactsSell: ''}},
// 4.75
//         {top: 1365, left: 1025, info: {priceSell: ' грн', contactsSell: ''}},
// 4.74
//         {top: 1365, left: 1065, info: {priceSell: ' грн', contactsSell: ''}},
// 4.73
//         {top: 1365, left: 1105, info: {priceSell: ' грн', contactsSell: ''}},
// 4.72
//         {top: 1365, left: 1145, info: {priceSell: ' грн', contactsSell: ''}},
// 4.71
//         {top: 1365, left: 1185, info: {priceSell: ' грн', contactsSell: ''}},
// 4.70
//         {top: 1365, left: 1225, info: {priceSell: ' грн', contactsSell: ''}},
// 4.69
//         {top: 1365, left: 1265, info: {priceSell: ' грн', contactsSell: ''}},
// 4.68
//         {top: 1365, left: 1305, info: {priceSell: ' грн', contactsSell: ''}},
// 4.67
//         {top: 1365, left: 1345, info: {priceSell: ' грн', contactsSell: ''}},
// 4.66
//         {top: 1365, left: 1385, info: {priceSell: ' грн', contactsSell: ''}},
// 4.65
//         {top: 1365, left: 1425, info: {priceSell: ' грн', contactsSell: ''}},
// 4.64
//         {top: 1365, left: 1465, info: {priceSell: ' грн', contactsSell: ''}},
// 4.63
//         {top: 1365, left: 1505, info: {priceSell: ' грн', contactsSell: ''}},
// 4.62
//         {top: 1365, left: 1545, info: {priceSell: ' грн', contactsSell: ''}},
// 4.61
//         {top: 1365, left: 1585, info: {priceSell: ' грн', contactsSell: ''}},
// 4.60
//         {top: 1365, left: 1625, info: {priceSell: ' грн', contactsSell: ''}},
// 4.59
//         {top: 1365, left: 1745, info: {priceSell: ' грн', contactsSell: ''}},
// 4.58
//         {top: 1365, left: 1785, info: {priceSell: ' грн', contactsSell: ''}},
// 4.57
//         {top: 1365, left: 1825, info: {priceSell: ' грн', contactsSell: ''}},
// 4.56
//         {top: 1365, left: 1865, info: {priceSell: ' грн', contactsSell: ''}},
// 4.55
//         {top: 1365, left: 1905, info: {priceSell: ' грн', contactsSell: ''}},
// 4.54
//         {top: 1365, left: 1945, info: {priceSell: ' грн', contactsSell: ''}},
// 4.53
//         {top: 1365, left: 1985, info: {priceSell: ' грн', contactsSell: ''}},
// 4.52
//         {top: 1365, left: 2025, info: {priceSell: ' грн', contactsSell: ''}},
// 4.51
//         {top: 1365, left: 2065, info: {priceSell: ' грн', contactsSell: ''}},
// 4.50
//         {top: 1365, left: 2105, info: {priceSell: ' грн', contactsSell: ''}},
// 4.49
//         {top: 1365, left: 2145, info: {priceSell: ' грн', contactsSell: ''}},
//
//
// 5 поверх - верх
// 5.01
//         {top: 1615, left: 265, info: {priceSell: ' грн', contactsSell: ''}},
// 5.02
//         {top: 1615, left: 305, info: {priceSell: ' грн', contactsSell: ''}},
// 5.03
//         {top: 1615, left: 345, info: {priceSell: ' грн', contactsSell: ''}},
// 5.04
//         {top: 1615, left: 385, info: {priceSell: ' грн', contactsSell: ''}},
// 5.05
//         {top: 1615, left: 425, info: {priceSell: ' грн', contactsSell: ''}},
// 5.06
//         {top: 1615, left: 465, info: {priceSell: ' грн', contactsSell: ''}},
// 5.07
//         {top: 1615, left: 505, info: {priceSell: ' грн', contactsSell: ''}},
// 5.08
//         {top: 1615, left: 545, info: {priceSell: ' грн', contactsSell: ''}},
// 5.09
//         {top: 1615, left: 585, info: {priceSell: ' грн', contactsSell: ''}},
// 5.10
//         {top: 1615, left: 625, info: {priceSell: ' грн', contactsSell: ''}},
// 5.11
//         {top: 1615, left: 665, info: {priceSell: ' грн', contactsSell: ''}},
// 5.12
//         {top: 1615, left: 705, info: {priceSell: ' грн', contactsSell: ''}},
// 5.13
//         {top: 1615, left: 745, info: {priceSell: ' грн', contactsSell: ''}},
// 5.14
//         {top: 1615, left: 785, info: {priceSell: ' грн', contactsSell: ''}},
// 5.15
//         {top: 1615, left: 825, info: {priceSell: ' грн', contactsSell: ''}},
// 5.16
//         {top: 1615, left: 865, info: {priceSell: ' грн', contactsSell: ''}},
// 5.17
//         {top: 1615, left: 905, info: {priceSell: ' грн', contactsSell: ''}},
// 5.18
//         {top: 1615, left: 945, info: {priceSell: ' грн', contactsSell: ''}},
// 5.19
//         {top: 1615, left: 985, info: {priceSell: ' грн', contactsSell: ''}},
// 5.20
//         {top: 1615, left: 1025, info: {priceSell: ' грн', contactsSell: ''}},
// 5.21
//         {top: 1615, left: 1065, info: {priceSell: ' грн', contactsSell: ''}},
// 5.22
//         {top: 1615, left: 1105, info: {priceSell: ' грн', contactsSell: ''}},
// 5.23
//         {top: 1615, left: 1145, info: {priceSell: ' грн', contactsSell: ''}},
// 5.24
//         {top: 1615, left: 1185, info: {priceSell: ' грн', contactsSell: ''}},
// 5.25
//         {top: 1615, left: 1225, info: {priceSell: ' грн', contactsSell: ''}},
// 5.26
//         {top: 1615, left: 1265, info: {priceSell: ' грн', contactsSell: ''}},
// 5.27
//         {top: 1615, left: 1305, info: {priceSell: ' грн', contactsSell: ''}},
// 5.28
//         {top: 1615, left: 1345, info: {priceSell: ' грн', contactsSell: ''}},
// 5.29
//         {top: 1615, left: 1385, info: {priceSell: ' грн', contactsSell: ''}},
// 5.30
//         {top: 1615, left: 1425, info: {priceSell: ' грн', contactsSell: ''}},
// 5.31
//         {top: 1615, left: 1465, info: {priceSell: ' грн', contactsSell: ''}},
// 5.32
//         {top: 1615, left: 1505, info: {priceSell: ' грн', contactsSell: ''}},
// 5.33
//         {top: 1615, left: 1545, info: {priceSell: ' грн', contactsSell: ''}},
// 5.34
//         {top: 1615, left: 1585, info: {priceSell: ' грн', contactsSell: ''}},
// 5.35
//         {top: 1615, left: 1625, info: {priceSell: ' грн', contactsSell: ''}},
// 5.36
//         {top: 1615, left: 1665, info: {priceSell: ' грн', contactsSell: ''}},
// 5.37
//         {top: 1615, left: 1705, info: {priceSell: ' грн', contactsSell: ''}},
// 5.38
//         {top: 1615, left: 1745, info: {priceSell: ' грн', contactsSell: ''}},
// 5.39
//         {top: 1615, left: 1785, info: {priceSell: ' грн', contactsSell: ''}},
// 5.40
//         {top: 1615, left: 1825, info: {priceSell: ' грн', contactsSell: ''}},
// 5.41
//         {top: 1615, left: 1865, info: {priceSell: ' грн', contactsSell: ''}},
// 5.42
//         {top: 1615, left: 1905, info: {priceSell: ' грн', contactsSell: ''}},
// 5.43
//         {top: 1615, left: 1945, info: {priceSell: ' грн', contactsSell: ''}},
// 5.44
//         {top: 1615, left: 1985, info: {priceSell: ' грн', contactsSell: ''}},
// 5.45
//         {top: 1615, left: 2025, info: {priceSell: ' грн', contactsSell: ''}},
// 5.46
//         {top: 1615, left: 2065, info: {priceSell: ' грн', contactsSell: ''}},
// 5.47
//         {top: 1615, left: 2105, info: {priceSell: ' грн', contactsSell: ''}},
// 5.48
//         {top: 1615, left: 2145, info: {priceSell: ' грн', contactsSell: ''}},
//
//
// 5 поверх - низ
// 5.92
//         {top: 1765, left: 265, info: {priceSell: ' грн', contactsSell: ''}},
// 5.91
//         {top: 1765, left: 305, info: {priceSell: ' грн', contactsSell: ''}},
// 5.90
//         {top: 1765, left: 345, info: {priceSell: ' грн', contactsSell: ''}},
// 5.89
//         {top: 1765, left: 385, info: {priceSell: ' грн', contactsSell: ''}},
// 5.88
//         {top: 1765, left: 425, info: {priceSell: ' грн', contactsSell: ''}},
// 5.87
//         {top: 1765, left: 465, info: {priceSell: ' грн', contactsSell: ''}},
// 5.86
//         {top: 1765, left: 505, info: {priceSell: ' грн', contactsSell: ''}},
// 5.85
//         {top: 1765, left: 545, info: {priceSell: ' грн', contactsSell: ''}},
// 5.84
//         {top: 1765, left: 585, info: {priceSell: ' грн', contactsSell: ''}},
// 5.83
//         {top: 1765, left: 625, info: {priceSell: ' грн', contactsSell: ''}},
// 5.82
//         {top: 1765, left: 665, info: {priceSell: ' грн', contactsSell: ''}},
// 5.81
//         {top: 1765, left: 785, info: {priceSell: ' грн', contactsSell: ''}},
// 5.80
//         {top: 1765, left: 825, info: {priceSell: ' грн', contactsSell: ''}},
// 5.79
//         {top: 1765, left: 865, info: {priceSell: ' грн', contactsSell: ''}},
// 5.78
//         {top: 1765, left: 905, info: {priceSell: ' грн', contactsSell: ''}},
// 5.77
//         {top: 1765, left: 945, info: {priceSell: ' грн', contactsSell: ''}},
// 5.76
//         {top: 1765, left: 985, info: {priceSell: ' грн', contactsSell: ''}},
// 5.75
//         {top: 1765, left: 1025, info: {priceSell: ' грн', contactsSell: ''}},
// 5.74
//         {top: 1765, left: 1065, info: {priceSell: ' грн', contactsSell: ''}},
// 5.73
//         {top: 1765, left: 1105, info: {priceSell: ' грн', contactsSell: ''}},
// 5.72
//         {top: 1765, left: 1145, info: {priceSell: ' грн', contactsSell: ''}},
// 5.71
//         {top: 1765, left: 1185, info: {priceSell: ' грн', contactsSell: ''}},
// 5.70
//         {top: 1765, left: 1225, info: {priceSell: ' грн', contactsSell: ''}},
// 5.69
//         {top: 1765, left: 1265, info: {priceSell: ' грн', contactsSell: ''}},
// 5.68
//         {top: 1765, left: 1305, info: {priceSell: ' грн', contactsSell: ''}},
// 5.67
//         {top: 1765, left: 1345, info: {priceSell: ' грн', contactsSell: ''}},
// 5.66
//         {top: 1765, left: 1385, info: {priceSell: ' грн', contactsSell: ''}},
// 5.65
//         {top: 1765, left: 1425, info: {priceSell: ' грн', contactsSell: ''}},
// 5.64
//         {top: 1765, left: 1465, info: {priceSell: ' грн', contactsSell: ''}},
// 5.63
//         {top: 1765, left: 1505, info: {priceSell: ' грн', contactsSell: ''}},
// 5.62
//         {top: 1765, left: 1545, info: {priceSell: ' грн', contactsSell: ''}},
// 5.61
//         {top: 1765, left: 1585, info: {priceSell: ' грн', contactsSell: ''}},
// 5.60
//         {top: 1765, left: 1625, info: {priceSell: ' грн', contactsSell: ''}},
// 5.59
//         {top: 1765, left: 1745, info: {priceSell: ' грн', contactsSell: ''}},
// 5.58
//         {top: 1765, left: 1785, info: {priceSell: ' грн', contactsSell: ''}},
// 5.57
//         {top: 1765, left: 1825, info: {priceSell: ' грн', contactsSell: ''}},
// 5.56
//         {top: 1765, left: 1865, info: {priceSell: ' грн', contactsSell: ''}},
// 5.55
        {top: 1765, left: 1905, info: {priceSell: '$15500', contactsSell: 'Telegram - +380674564659'}},
// 5.54
//         {top: 1765, left: 1945, info: {priceSell: ' грн', contactsSell: ''}},
// 5.53
//         {top: 1765, left: 1985, info: {priceSell: ' грн', contactsSell: ''}},
// 5.52
//         {top: 1765, left: 2025, info: {priceSell: ' грн', contactsSell: ''}},
// 5.51
//         {top: 1765, left: 2065, info: {priceSell: ' грн', contactsSell: ''}},
// 5.50
//         {top: 1765, left: 2105, info: {priceSell: ' грн', contactsSell: ''}},
// 5.49
//         {top: 1765, left: 2145, info: {priceSell: ' грн', contactsSell: ''}},
//
//
// 6 поверх - верх
// 6.01
//         {top: 2015, left: 265, info: {priceSell: ' грн', contactsSell: ''}},
// 6.02
//         {top: 2015, left: 305, info: {priceSell: ' грн', contactsSell: ''}},
// 6.03
//         {top: 2015, left: 345, info: {priceSell: ' грн', contactsSell: ''}},
// 6.04
//         {top: 2015, left: 385, info: {priceSell: ' грн', contactsSell: ''}},
// 6.05
//         {top: 2015, left: 425, info: {priceSell: ' грн', contactsSell: ''}},
// 6.06
//         {top: 2015, left: 465, info: {priceSell: ' грн', contactsSell: ''}},
// 6.07
//         {top: 2015, left: 505, info: {priceSell: ' грн', contactsSell: ''}},
// 6.08
//         {top: 2015, left: 545, info: {priceSell: ' грн', contactsSell: ''}},
// 6.09
//         {top: 2015, left: 585, info: {priceSell: ' грн', contactsSell: ''}},
// 6.10
//         {top: 2015, left: 625, info: {priceSell: ' грн', contactsSell: ''}},
// 6.11
//         {top: 2015, left: 665, info: {priceSell: ' грн', contactsSell: ''}},
// 6.12
//         {top: 2015, left: 705, info: {priceSell: ' грн', contactsSell: ''}},
// 6.13
//         {top: 2015, left: 745, info: {priceSell: ' грн', contactsSell: ''}},
// 6.14
//         {top: 2015, left: 785, info: {priceSell: ' грн', contactsSell: ''}},
// 6.15
//         {top: 2015, left: 825, info: {priceSell: ' грн', contactsSell: ''}},
// 6.16
//         {top: 2015, left: 865, info: {priceSell: ' грн', contactsSell: ''}},
// 6.17
//         {top: 2015, left: 905, info: {priceSell: ' грн', contactsSell: ''}},
// 6.18
//         {top: 2015, left: 945, info: {priceSell: ' грн', contactsSell: ''}},
// 6.19
//         {top: 2015, left: 985, info: {priceSell: ' грн', contactsSell: ''}},
// 6.20
//         {top: 2015, left: 1025, info: {priceSell: ' грн', contactsSell: ''}},
// 6.21
//         {top: 2015, left: 1065, info: {priceSell: ' грн', contactsSell: ''}},
// 6.22
//         {top: 2015, left: 1105, info: {priceSell: ' грн', contactsSell: ''}},
// 6.23
//         {top: 2015, left: 1145, info: {priceSell: ' грн', contactsSell: ''}},
// 6.24
//         {top: 2015, left: 1185, info: {priceSell: ' грн', contactsSell: ''}},
// 6.25
//         {top: 2015, left: 1225, info: {priceSell: ' грн', contactsSell: ''}},
// 6.26
//         {top: 2015, left: 1265, info: {priceSell: ' грн', contactsSell: ''}},
// 6.27
//         {top: 2015, left: 1305, info: {priceSell: ' грн', contactsSell: ''}},
// 6.28
//         {top: 2015, left: 1345, info: {priceSell: ' грн', contactsSell: ''}},
// 6.29
//         {top: 2015, left: 1385, info: {priceSell: ' грн', contactsSell: ''}},
// 6.30
//         {top: 2015, left: 1425, info: {priceSell: ' грн', contactsSell: ''}},
// 6.31
//         {top: 2015, left: 1465, info: {priceSell: ' грн', contactsSell: ''}},
// 6.32
//         {top: 2015, left: 1505, info: {priceSell: ' грн', contactsSell: ''}},
// 6.33
//         {top: 2015, left: 1545, info: {priceSell: '2500грн + комуналка', contacts: '0973624632'}},
// 6.34
//         {top: 2015, left: 1585, info: {priceSell: ' грн', contactsSell: ''}},
// 6.35
//         {top: 2015, left: 1625, info: {priceSell: ' грн', contactsSell: ''}},
// 6.36
//         {top: 2015, left: 1665, info: {priceSell: ' грн', contactsSell: ''}},
// 6.37
//         {top: 2015, left: 1705, info: {priceSell: ' грн', contactsSell: ''}},
// 6.38
//         {top: 2015, left: 1745, info: {priceSell: ' грн', contactsSell: ''}},
// 6.39
//         {top: 2015, left: 1785, info: {priceSell: ' грн', contactsSell: ''}},
// 6.40
//         {top: 2015, left: 1825, info: {priceSell: ' грн', contactsSell: ''}},
// 6.41
//         {top: 2015, left: 1865, info: {priceSell: ' грн', contactsSell: ''}},
// 6.42
//         {top: 2015, left: 1905, info: {priceSell: ' грн', contactsSell: ''}},
// 6.43
//         {top: 2015, left: 1945, info: {priceSell: ' грн', contactsSell: ''}},
// 6.44
//         {top: 2015, left: 1985, info: {priceSell: ' грн', contactsSell: ''}},
// 6.45
//         {top: 2015, left: 2025, info: {priceSell: ' грн', contactsSell: ''}},
// 6.46
//         {top: 2015, left: 2065, info: {priceSell: ' грн', contactsSell: ''}},
// 6.47
//         {top: 2015, left: 2105, info: {priceSell: ' грн', contactsSell: ''}},
// 6.48
//         {top: 2015, left: 2145, info: {priceSell: ' грн', contactsSell: ''}},
//
//
// 6 поверх - низ
// 6.92
//         {top: 2165, left: 265, info: {priceSell: ' грн', contactsSell: ''}},
// 6.91
//         {top: 2165, left: 305, info: {priceSell: ' грн', contactsSell: ''}},
// 6.90
//         {top: 2165, left: 345, info: {priceSell: ' грн', contactsSell: ''}},
// 6.89
//         {top: 2165, left: 385, info: {priceSell: ' грн', contactsSell: ''}},
// 6.88
//         {top: 2165, left: 425, info: {priceSell: ' грн', contactsSell: ''}},
// 6.87
//         {top: 2165, left: 465, info: {priceSell: ' грн', contactsSell: ''}},
// 6.86
//         {top: 2165, left: 505, info: {priceSell: ' грн', contactsSell: ''}},
// 6.85
//         {top: 2165, left: 545, info: {priceSell: ' грн', contactsSell: ''}},
// 6.84
//         {top: 2165, left: 585, info: {priceSell: ' грн', contactsSell: ''}},
// 6.83
//         {top: 2165, left: 625, info: {priceSell: ' грн', contactsSell: ''}},
// 6.82
//         {top: 2165, left: 665, info: {priceSell: ' грн', contactsSell: ''}},
// 6.81
//         {top: 2165, left: 785, info: {priceSell: ' грн', contactsSell: ''}},
// 6.80
//         {top: 2165, left: 825, info: {priceSell: ' грн', contactsSell: ''}},
// 6.79
//         {top: 2165, left: 865, info: {priceSell: ' грн', contactsSell: ''}},
// 6.78
//         {top: 2165, left: 905, info: {priceSell: ' грн', contactsSell: ''}},
// 6.77
//         {top: 2165, left: 945, info: {priceSell: ' грн', contactsSell: ''}},
// 6.76
//         {top: 2165, left: 985, info: {priceSell: ' грн', contactsSell: ''}},
// 6.75
//         {top: 2165, left: 1025, info: {priceSell: ' грн', contactsSell: ''}},
// 6.74
//         {top: 2165, left: 1065, info: {priceSell: ' грн', contactsSell: ''}},
// 6.73
//         {top: 2165, left: 1105, info: {priceSell: ' грн', contactsSell: ''}},
// 6.72
//         {top: 2165, left: 1145, info: {priceSell: ' грн', contactsSell: ''}},
// 6.71
//         {top: 2165, left: 1185, info: {priceSell: ' грн', contactsSell: ''}},
// 6.70
//         {top: 2165, left: 1225, info: {priceSell: ' грн', contactsSell: ''}},
// 6.69
//         {top: 2165, left: 1265, info: {priceSell: ' грн', contactsSell: ''}},
// 6.68
//         {top: 2165, left: 1305, info: {priceSell: ' грн', contactsSell: ''}},
// 6.67
//         {top: 2165, left: 1345, info: {priceSell: ' грн', contactsSell: ''}},
// 6.66
//         {top: 2165, left: 1385, info: {priceSell: ' грн', contactsSell: ''}},
// 6.65
//         {top: 2165, left: 1425, info: {priceSell: ' грн', contactsSell: ''}},
// 6.64
//         {top: 2165, left: 1465, info: {priceSell: ' грн', contactsSell: ''}},
// 6.63
//         {top: 2165, left: 1505, info: {priceSell: ' грн', contactsSell: ''}},
// 6.62
//         {top: 2165, left: 1545, info: {priceSell: ' грн', contactsSell: ''}},
// 6.61
//         {top: 2165, left: 1585, info: {priceSell: ' грн', contactsSell: ''}},
// 6.60
//         {top: 2165, left: 1625, info: {priceSell: ' грн', contactsSell: ''}},
// 6.59
//         {top: 2165, left: 1745, info: {priceSell: ' грн', contactsSell: ''}},
// 6.58
//         {top: 2165, left: 1785, info: {priceSell: ' грн', contactsSell: ''}},
// 6.57
//         {top: 2165, left: 1825, info: {priceSell: ' грн', contactsSell: ''}},
// 6.56
//         {top: 2165, left: 1865, info: {priceSell: ' грн', contactsSell: ''}},
// 6.55
//         {top: 2165, left: 1905, info: {priceSell: ' грн', contactsSell: ''}},
// 6.54
//         {top: 2165, left: 1945, info: {priceSell: ' грн', contactsSell: ''}},
// 6.53
//         {top: 2165, left: 1985, info: {priceSell: ' грн', contactsSell: ''}},
// 6.52
//         {top: 2165, left: 2025, info: {priceSell: ' грн', contactsSell: ''}},
// 6.51
//         {top: 2165, left: 2065, info: {priceSell: ' грн', contactsSell: ''}},
// 6.50
//         {top: 2165, left: 2105, info: {priceSell: ' грн', contactsSell: ''}},
// 6.49
//         {top: 2165, left: 2145, info: {priceSell: ' грн', contacts: ''}}

    ];

    // Додаємо іконки на сторінку
    iconCoordinates.forEach(function (coordinates) {
        var icon = document.createElement('div');
        icon.className = 'icon';
        icon.innerHTML = '&#128178;';
        icon.style.top = coordinates.top + 'px';
        icon.style.left = coordinates.left + 'px';
        iconsContainer.appendChild(icon);

        // Додаємо обробник кліку для кожної іконки
        icon.addEventListener('click', function () {
            // Показуємо попап
            sell.style.display = 'block';

            // Встановлюємо інформацію в попап
            priceSell.textContent = 'Ціна: ' + coordinates.info.priceSell;
            contactsSell.textContent = 'Контакти: ' + coordinates.info.contactsSell;

            // Встановлюємо позицію попапа над іконкою
            sell.style.top = (coordinates.top + 50) + 'px'; // Змінюйте потрібну відстань від іконки
            sell.style.left = (coordinates.left - 40) + 'px'; // Змінюйте потрібну відстань від іконки
        });
    });
});

// Функція для закриття попапа
function closeSell() {
    document.getElementById('sell').style.display = 'none';
}