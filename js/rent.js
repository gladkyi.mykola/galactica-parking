document.addEventListener('DOMContentLoaded', function () {
    var iconsContainer = document.getElementById('icons-container');
    var popup = document.getElementById('popup');
    var price = document.getElementById('price');
    var contacts = document.getElementById('contacts')

    var iconCoordinates = [
//
// 1 поверх - верх
// 1.01 
//         {top: 40, left: 455, info: {price: '1000 грн + комуналка', contacts: ''}},
// 1.02 
//         {top: 40, left: 505, info: {price: ' грн', contacts: ''}},
// 1.03 
//         {top: 40, left: 545, info: {price: ' грн', contacts: ''}},
// 1.04 
//         {top: 40, left: 585, info: {price: ' грн', contacts: ''}},
// 1.05 
//         {top: 40, left: 625, info: {price: ' грн', contacts: ''}},
// 1.06 
//         {top: 40, left: 665, info: {price: ' грн', contacts: ''}},
// 1.07 
//         {top: 40, left: 705, info: {price: ' грн', contacts: ''}},
// 1.08 
//         {top: 40, left: 745, info: {price: ' грн', contacts: ''}},
// 1.09 
//         {top: 40, left: 785, info: {price: ' грн', contacts: ''}},
// 1.10 
//         {top: 40, left: 825, info: {price: ' грн', contacts: ''}},
// 1.11 
//         {top: 40, left: 865, info: {price: ' грн', contacts: ''}},
// 1.12 
//         {top: 40, left: 905, info: {price: ' грн', contacts: ''}},
// 1.13 
//         {top: 40, left: 945, info: {price: ' грн', contacts: ''}},
// 1.14 
//         {top: 40, left: 985, info: {price: ' грн', contacts: ''}},
// 1.15 
//         {top: 40, left: 1025, info: {price: ' грн', contacts: ''}},
// 1.16 
//         {top: 40, left: 1065, info: {price: ' грн', contacts: ''}},
// 1.17 
//         {top: 40, left: 1105, info: {price: ' грн', contacts: ''}},
// 1.18 
//         {top: 40, left: 1145, info: {price: ' грн', contacts: ''}},
// 1.19 
//         {top: 40, left: 1185, info: {price: ' грн', contacts: ''}},
// 1.20 
//         {top: 40, left: 1225, info: {price: ' грн', contacts: ''}},
// 1.21 
//         {top: 40, left: 1265, info: {price: ' грн', contacts: ''}},
// 1.22 
//         {top: 40, left: 1305, info: {price: ' грн', contacts: ''}},
// 1.23 
//         {top: 40, left: 1345, info: {price: ' грн', contacts: ''}},
// 1.24 
//         {top: 40, left: 1385, info: {price: ' грн', contacts: ''}},
// 1.25 
//         {top: 40, left: 1425, info: {price: ' грн', contacts: ''}},
// 1.26 
//         {top: 40, left: 1465, info: {price: ' грн', contacts: ''}},
// 1.27 
//         {top: 40, left: 1505, info: {price: ' грн', contacts: ''}},
// 1.28 
//         {top: 40, left: 1545, info: {price: ' грн', contacts: ''}},
// 1.29 
//         {top: 40, left: 1585, info: {price: ' грн', contacts: ''}},
// 1.30 
//         {top: 40, left: 1625, info: {price: ' грн', contacts: ''}},
// 1.31 
//         {top: 40, left: 1665, info: {price: ' грн', contacts: ''}},
// 1.32 
//         {top: 40, left: 1705, info: {price: ' грн', contacts: ''}},
// 1.33 
//         {top: 40, left: 1745, info: {price: ' грн', contacts: ''}},
// 1.34 
//         {top: 40, left: 1785, info: {price: ' грн', contacts: ''}},
// 1.35 
//         {top: 40, left: 1825, info: {price: ' грн', contacts: ''}},
// 1.36 
//         {top: 40, left: 1865, info: {price: ' грн', contacts: ''}},
// 1.37 
//         {top: 40, left: 1905, info: {price: ' грн', contacts: ''}},
// 1.38 
//         {top: 40, left: 1955, info: {price: ' грн', contacts: ''}},
//
// 1 поверх - низ
// 1.78
//         {top: 190, left: 265, info: {price: ' грн', contacts: ''}},
// 1.77
//         {top: 190, left: 305, info: {price: ' грн', contacts: ''}},
// 1.76
//         {top: 190, left: 345, info: {price: ' грн', contacts: ''}},
// 1.75
//         {top: 190, left: 385, info: {price: ' грн', contacts: ''}},
// 1.74
//         {top: 190, left: 425, info: {price: ' грн', contacts: ''}},
// 1.73
//         {top: 190, left: 465, info: {price: ' грн', contacts: ''}},
// 1.72
//         {top: 190, left: 505, info: {price: ' грн', contacts: ''}},
// 1.71
//         {top: 190, left: 545, info: {price: ' грн', contacts: ''}},
// 1.70
//         {top: 190, left: 585, info: {price: ' грн', contacts: ''}},
// 1.69
//         {top: 190, left: 635, info: {price: ' грн', contacts: ''}},
// 1.68
//         {top: 190, left: 855, info: {price: ' грн', contacts: ''}},
// 1.67
//         {top: 190, left: 905, info: {price: ' грн', contacts: ''}},
// 1.66
//         {top: 190, left: 945, info: {price: ' грн', contacts: ''}},
// 1.65
//         {top: 190, left: 985, info: {price: ' грн', contacts: ''}},
// 1.64
//         {top: 190, left: 1025, info: {price: ' грн', contacts: ''}},
// 1.63
//         {top: 190, left: 1065, info: {price: ' грн', contacts: ''}},
// 1.62
//         {top: 190, left: 1105, info: {price: ' грн', contacts: ''}},
// 1.61
//         {top: 190, left: 1145, info: {price: ' грн', contacts: ''}},
// 1.60
//         {top: 190, left: 1185, info: {price: ' грн', contacts: ''}},
// 1.59
//         {top: 190, left: 1225, info: {price: ' грн', contacts: ''}},
// 1.58
//         {top: 190, left: 1265, info: {price: ' грн', contacts: ''}},
// 1.57
//         {top: 190, left: 1305, info: {price: ' грн', contacts: ''}},
// 1.56
//         {top: 190, left: 1345, info: {price: ' грн', contacts: ''}},
// 1.55
//         {top: 190, left: 1385, info: {price: ' грн', contacts: ''}},
// 1.54
//         {top: 190, left: 1425, info: {price: ' грн', contacts: ''}},
// 1.53
//         {top: 190, left: 1465, info: {price: ' грн', contacts: ''}},
// 1.52
//         {top: 190, left: 1505, info: {price: ' грн', contacts: ''}},
// 1.51
//         {top: 190, left: 1545, info: {price: ' грн', contacts: ''}},
// 1.50
//         {top: 190, left: 1585, info: {price: ' грн', contacts: ''}},
// 1.49
//         {top: 190, left: 1625, info: {price: ' грн', contacts: ''}},
// 1.48
//         {top: 190, left: 1780, info: {price: ' грн', contacts: ''}},
// 1.47
//         {top: 190, left: 1825, info: {price: ' грн', contacts: ''}},
// 1.46
//         {top: 190, left: 1865, info: {price: ' грн', contacts: ''}},
// 1.45
//         {top: 190, left: 1905, info: {price: ' грн', contacts: ''}},
// 1.44
//         {top: 190, left: 1945, info: {price: ' грн', contacts: ''}},
// 1.43
//         {top: 190, left: 1985, info: {price: ' грн', contacts: ''}},
// 1.42
//         {top: 190, left: 2025, info: {price: ' грн', contacts: ''}},
// 1.41
//         {top: 190, left: 2065, info: {price: ' грн', contacts: ''}},
// 1.40
//         {top: 190, left: 2105, info: {price: ' грн', contacts: ''}},
// 1.39
//         {top: 190, left: 2145, info: {price: ' грн', contacts: ''}},
//
//
// 2 поверх - верх
// 2.01
//         {top: 415, left: 265, info: {price: ' грн', contacts: ''}},
// 2.02
//         {top: 415, left: 305, info: {price: ' грн', contacts: ''}},
// 2.03
//         {top: 415, left: 345, info: {price: ' грн', contacts: ''}},
// 2.04
//         {top: 415, left: 385, info: {price: ' грн', contacts: ''}},
// 2.05
//         {top: 415, left: 425, info: {price: ' грн', contacts: ''}},
// 2.06
//         {top: 415, left: 465, info: {price: ' грн', contacts: ''}},
// 2.07
//         {top: 415, left: 505, info: {price: ' грн', contacts: ''}},
// 2.08
//         {top: 415, left: 545, info: {price: ' грн', contacts: ''}},
// 2.09
//         {top: 415, left: 585, info: {price: ' грн', contacts: ''}},
// 2.10
//         {top: 415, left: 625, info: {price: ' грн', contacts: ''}},
// 2.11
//         {top: 415, left: 665, info: {price: ' грн', contacts: ''}},
// 2.12
//         {top: 415, left: 705, info: {price: ' грн', contacts: ''}},
// 2.13
//         {top: 415, left: 745, info: {price: ' грн', contacts: ''}},
// 2.14
//         {top: 415, left: 785, info: {price: ' грн', contacts: ''}},
// 2.15
//         {top: 415, left: 825, info: {price: ' грн', contacts: ''}},
// 2.16
//         {top: 415, left: 865, info: {price: ' грн', contacts: ''}},
// 2.17
//         {top: 415, left: 905, info: {price: ' грн', contacts: ''}},
// 2.18
//         {top: 415, left: 945, info: {price: ' грн', contacts: ''}},
// 2.19
//         {top: 415, left: 985, info: {price: ' грн', contacts: ''}},
// 2.20
//         {top: 415, left: 1025, info: {price: ' грн', contacts: ''}},
// 2.21
//         {top: 415, left: 1065, info: {price: ' грн', contacts: ''}},
// 2.22
//         {top: 415, left: 1105, info: {price: ' грн', contacts: ''}},
// 2.23
//         {top: 415, left: 1145, info: {price: ' грн', contacts: ''}},
// 2.24
//         {top: 415, left: 1185, info: {price: ' грн', contacts: ''}},
// 2.25
//         {top: 415, left: 1225, info: {price: ' грн', contacts: ''}},
// 2.26
//         {top: 415, left: 1265, info: {price: ' грн', contacts: ''}},
// 2.27
//         {top: 415, left: 1305, info: {price: ' грн', contacts: ''}},
// 2.28
//         {top: 415, left: 1345, info: {price: ' грн', contacts: ''}},
// 2.29
//         {top: 415, left: 1385, info: {price: ' грн', contacts: ''}},
// 2.30
//         {top: 415, left: 1425, info: {price: ' грн', contacts: ''}},
// 2.31
//         {top: 415, left: 1465, info: {price: ' грн', contacts: ''}},
// 2.32
//         {top: 415, left: 1505, info: {price: ' грн', contacts: ''}},
// 2.33
//         {top: 415, left: 1545, info: {price: ' грн', contacts: ''}},
// 2.34
//         {top: 415, left: 1585, info: {price: ' грн', contacts: ''}},
// 2.35
//         {top: 415, left: 1625, info: {price: ' грн', contacts: ''}},
// 2.36
//         {top: 415, left: 1665, info: {price: ' грн', contacts: ''}},
// 2.37
//         {top: 415, left: 1705, info: {price: ' грн', contacts: ''}},
// 2.38
//         {top: 415, left: 1745, info: {price: ' грн', contacts: ''}},
// 2.39
//         {top: 415, left: 1785, info: {price: ' грн', contacts: ''}},
// 2.40
//         {top: 415, left: 1825, info: {price: ' грн', contacts: ''}},
// 2.41
//         {top: 415, left: 1865, info: {price: ' грн', contacts: ''}},
// 2.42
//         {top: 415, left: 1905, info: {price: ' грн', contacts: ''}},
// 2.43
//         {top: 415, left: 1945, info: {price: ' грн', contacts: ''}},
// 2.44
//         {top: 415, left: 1985, info: {price: ' грн', contacts: ''}},
// 2.45
//         {top: 415, left: 2025, info: {price: ' грн', contacts: ''}},
// 2.46
//         {top: 415, left: 2065, info: {price: ' грн', contacts: ''}},
// 2.47
//         {top: 415, left: 2105, info: {price: ' грн', contacts: ''}},
// 2.48
//         {top: 415, left: 2145, info: {price: ' грн', contacts: ''}},
//
//
// 2 поверх - низ
// 2.92
//         {top: 565, left: 265, info: {price: ' грн', contacts: ''}},
// 2.91
//         {top: 565, left: 305, info: {price: ' грн', contacts: ''}},
// 2.90
//         {top: 565, left: 345, info: {price: ' грн', contacts: ''}},
// 2.89
//         {top: 565, left: 385, info: {price: ' грн', contacts: ''}},
// 2.88
//         {top: 565, left: 425, info: {price: ' грн', contacts: ''}},
// 2.87
//         {top: 565, left: 465, info: {price: ' грн', contacts: ''}},
// 2.86
//         {top: 565, left: 505, info: {price: ' грн', contacts: ''}},
// 2.85
//         {top: 565, left: 545, info: {price: ' грн', contacts: ''}},
// 2.84
//         {top: 565, left: 585, info: {price: ' грн', contacts: ''}},
// 2.83
//         {top: 565, left: 625, info: {price: ' грн', contacts: ''}},
// 2.82
//         {top: 565, left: 665, info: {price: ' грн', contacts: ''}},
// 2.81
//         {top: 565, left: 785, info: {price: ' грн', contacts: ''}},
// 2.80
//         {top: 565, left: 825, info: {price: ' грн', contacts: ''}},
// 2.79
//         {top: 565, left: 865, info: {price: ' грн', contacts: ''}},
// 2.78
//         {top: 565, left: 905, info: {price: ' грн', contacts: ''}},
// 2.77
//         {top: 565, left: 945, info: {price: ' грн', contacts: ''}},
// 2.76
//         {top: 565, left: 985, info: {price: ' грн', contacts: ''}},
// 2.75
//         {top: 565, left: 1025, info: {price: ' грн', contacts: ''}},
// 2.74
//         {top: 565, left: 1065, info: {price: ' грн', contacts: ''}},
// 2.73
//         {top: 565, left: 1105, info: {price: ' грн', contacts: ''}},
// 2.72
//         {top: 565, left: 1145, info: {price: ' грн', contacts: ''}},
// 2.71
//         {top: 565, left: 1185, info: {price: ' грн', contacts: ''}},
// 2.70
//         {top: 565, left: 1225, info: {price: ' грн', contacts: ''}},
// 2.69
//         {top: 565, left: 1265, info: {price: ' грн', contacts: ''}},
// 2.68
//         {top: 565, left: 1305, info: {price: ' грн', contacts: ''}},
// 2.67
//         {top: 565, left: 1345, info: {price: ' грн', contacts: ''}},
// 2.66
//         {top: 565, left: 1385, info: {price: ' грн', contacts: ''}},
// 2.65
//         {top: 565, left: 1425, info: {price: ' грн', contacts: ''}},
// 2.64
//         {top: 565, left: 1465, info: {price: ' грн', contacts: ''}},
// 2.63
//         {top: 565, left: 1505, info: {price: ' грн', contacts: ''}},
// 2.62
//         {top: 565, left: 1545, info: {price: ' грн', contacts: ''}},
// 2.61
//         {top: 565, left: 1585, info: {price: ' грн', contacts: ''}},
// 2.60
        {top: 565, left: 1625, info: {price: '3000 грн + комуналка', contacts: '067-504-1496'}},
// 2.59
//         {top: 565, left: 1745, info: {price: ' грн', contacts: ''}},
// 2.58
//         {top: 565, left: 1785, info: {price: ' грн', contacts: ''}},
// 2.57
//         {top: 565, left: 1825, info: {price: ' грн', contacts: ''}},
// 2.56
//         {top: 565, left: 1865, info: {price: ' грн', contacts: ''}},
// 2.55
//         {top: 565, left: 1905, info: {price: ' грн', contacts: ''}},
// 2.54
//         {top: 565, left: 1945, info: {price: ' грн', contacts: ''}},
// 2.53
//         {top: 565, left: 1985, info: {price: ' грн', contacts: ''}},
// 2.52
//         {top: 565, left: 2025, info: {price: ' грн', contacts: ''}},
// 2.51
//         {top: 565, left: 2065, info: {price: ' грн', contacts: ''}},
// 2.50
//         {top: 565, left: 2105, info: {price: ' грн', contacts: ''}},
// 2.49
//         {top: 565, left: 2145, info: {price: ' грн', contacts: ''}},
//
//
// 3 поверх - верх
// 3.01
//         {top: 815, left: 265, info: {price: ' грн', contacts: ''}},
// 3.02
//         {top: 815, left: 305, info: {price: ' грн', contacts: ''}},
// 3.03
//         {top: 815, left: 345, info: {price: ' грн', contacts: ''}},
// 3.04
//         {top: 815, left: 385, info: {price: ' грн', contacts: ''}},
// 3.05
//         {top: 815, left: 425, info: {price: ' грн', contacts: ''}},
// 3.06
//         {top: 815, left: 465, info: {price: ' грн', contacts: ''}},
// 3.07
//         {top: 815, left: 505, info: {price: ' грн', contacts: ''}},
// 3.08
//         {top: 815, left: 545, info: {price: ' грн', contacts: ''}},
// 3.09
//         {top: 815, left: 585, info: {price: ' грн', contacts: ''}},
// 3.10
//         {top: 815, left: 625, info: {price: ' грн', contacts: ''}},
// 3.11
//         {top: 815, left: 665, info: {price: ' грн', contacts: ''}},
// 3.12
//         {top: 815, left: 705, info: {price: ' грн', contacts: ''}},
// 3.13
//         {top: 815, left: 745, info: {price: ' грн', contacts: ''}},
// 3.14
//         {top: 815, left: 785, info: {price: ' грн', contacts: ''}},
// 3.15
//         {top: 815, left: 825, info: {price: ' грн', contacts: ''}},
// 3.16
//         {top: 815, left: 865, info: {price: ' грн', contacts: ''}},
// 3.17
//         {top: 815, left: 905, info: {price: ' грн', contacts: ''}},
// 3.18
//         {top: 815, left: 945, info: {price: ' грн', contacts: ''}},
// 3.19
//         {top: 815, left: 985, info: {price: ' грн', contacts: ''}},
// 3.20
//         {top: 815, left: 1025, info: {price: ' грн', contacts: ''}},
// 3.21
//         {top: 815, left: 1065, info: {price: ' грн', contacts: ''}},
// 3.22
//         {top: 815, left: 1105, info: {price: ' грн', contacts: ''}},
// 3.23
//         {top: 815, left: 1145, info: {price: ' грн', contacts: ''}},
// 3.24
//         {top: 815, left: 1185, info: {price: ' грн', contacts: ''}},
// 3.25
//         {top: 815, left: 1225, info: {price: ' грн', contacts: ''}},
// 3.26
//         {top: 815, left: 1265, info: {price: ' грн', contacts: ''}},
// 3.27
//         {top: 815, left: 1305, info: {price: ' грн', contacts: ''}},
// 3.28
//         {top: 815, left: 1345, info: {price: ' грн', contacts: ''}},
// 3.29
//         {top: 815, left: 1385, info: {price: ' грн', contacts: ''}},
// 3.30
//         {top: 815, left: 1425, info: {price: ' грн', contacts: ''}},
// 3.31
//         {top: 815, left: 1465, info: {price: ' грн', contacts: ''}},
// 3.32
//         {top: 815, left: 1505, info: {price: ' грн', contacts: ''}},
// 3.33
//         {top: 815, left: 1545, info: {price: ' грн', contacts: ''}},
// 3.34
//         {top: 815, left: 1585, info: {price: ' грн', contacts: ''}},
// 3.35
//         {top: 815, left: 1625, info: {price: ' грн', contacts: ''}},
// 3.36
//         {top: 815, left: 1665, info: {price: ' грн', contacts: ''}},
// 3.37
//         {top: 815, left: 1705, info: {price: ' грн', contacts: ''}},
// 3.38
//         {top: 815, left: 1745, info: {price: ' грн', contacts: ''}},
// 3.39
//         {top: 815, left: 1785, info: {price: ' грн', contacts: ''}},
// 3.40
//         {top: 815, left: 1825, info: {price: ' грн', contacts: ''}},
// 3.41
//         {top: 815, left: 1865, info: {price: ' грн', contacts: ''}},
// 3.42
//         {top: 815, left: 1905, info: {price: ' грн', contacts: ''}},
// 3.43
//         {top: 815, left: 1945, info: {price: ' грн', contacts: ''}},
// 3.44
//         {top: 815, left: 1985, info: {price: ' грн', contacts: ''}},
// 3.45
//         {top: 815, left: 2025, info: {price: ' грн', contacts: ''}},
// 3.46
//         {top: 815, left: 2065, info: {price: ' грн', contacts: ''}},
// 3.47
//         {top: 815, left: 2105, info: {price: ' грн', contacts: ''}},
// 3.48
//         {top: 815, left: 2145, info: {price: ' грн', contacts: ''}},
//
//
// 3 поверх - низ
// 3.92
//         {top: 965, left: 265, info: {price: ' грн', contacts: ''}},
// 3.91
//         {top: 965, left: 305, info: {price: ' грн', contacts: ''}},
// 3.90
//         {top: 965, left: 345, info: {price: ' грн', contacts: ''}},
// 3.89
//         {top: 965, left: 385, info: {price: ' грн', contacts: ''}},
// 3.88
//         {top: 965, left: 425, info: {price: ' грн', contacts: ''}},
// 3.87
//         {top: 965, left: 465, info: {price: ' грн', contacts: ''}},
// 3.86
//         {top: 965, left: 505, info: {price: ' грн', contacts: ''}},
// 3.85
//         {top: 965, left: 545, info: {price: ' грн', contacts: ''}},
// 3.84
//         {top: 965, left: 585, info: {price: ' грн', contacts: ''}},
// 3.83
//         {top: 965, left: 625, info: {price: ' грн', contacts: ''}},
// 3.82
//         {top: 965, left: 665, info: {price: ' грн', contacts: ''}},
// 3.81
//         {top: 965, left: 785, info: {price: ' грн', contacts: ''}},
// 3.80
//         {top: 965, left: 825, info: {price: ' грн', contacts: ''}},
// 3.79
//         {top: 965, left: 865, info: {price: ' грн', contacts: ''}},
// 3.78
//         {top: 965, left: 905, info: {price: ' грн', contacts: ''}},
// 3.77
//         {top: 965, left: 945, info: {price: ' грн', contacts: ''}},
// 3.76
//         {top: 965, left: 985, info: {price: ' грн', contacts: ''}},
// 3.75
//         {top: 965, left: 1025, info: {price: ' грн', contacts: ''}},
// 3.74
//         {top: 965, left: 1065, info: {price: ' грн', contacts: ''}},
// 3.73
//         {top: 965, left: 1105, info: {price: ' грн', contacts: ''}},
// 3.72
//         {top: 965, left: 1145, info: {price: ' грн', contacts: ''}},
// 3.71
//         {top: 965, left: 1185, info: {price: ' грн', contacts: ''}},
// 3.70
//         {top: 965, left: 1225, info: {price: ' грн', contacts: ''}},
// 3.69
//         {top: 965, left: 1265, info: {price: ' грн', contacts: ''}},
// 3.68
//         {top: 965, left: 1305, info: {price: ' грн', contacts: ''}},
// 3.67
//         {top: 965, left: 1345, info: {price: ' грн', contacts: ''}},
// 3.66
//         {top: 965, left: 1385, info: {price: ' грн', contacts: ''}},
// 3.65
//         {top: 965, left: 1425, info: {price: ' грн', contacts: ''}},
// 3.64
//         {top: 965, left: 1465, info: {price: ' грн', contacts: ''}},
// 3.63
//         {top: 965, left: 1505, info: {price: ' грн', contacts: ''}},
// 3.62
//         {top: 965, left: 1545, info: {price: ' грн', contacts: ''}},
// 3.61
//         {top: 965, left: 1585, info: {price: ' грн', contacts: ''}},
// 3.60
//         {top: 965, left: 1625, info: {price: ' грн', contacts: ''}},
// 3.59
//         {top: 965, left: 1745, info: {price: ' грн', contacts: ''}},
// 3.58
//         {top: 965, left: 1785, info: {price: ' грн', contacts: ''}},
// 3.57
//         {top: 965, left: 1825, info: {price: ' грн', contacts: ''}},
// 3.56
//         {top: 965, left: 1865, info: {price: ' грн', contacts: ''}},
// 3.55
//         {top: 965, left: 1905, info: {price: ' грн', contacts: ''}},
// 3.54
        {top: 965, left: 1945, info: {price: '2000грн + комуналка', contacts: '093-998-2011'}},
// 3.53
//         {top: 965, left: 1985, info: {price: ' грн', contacts: ''}},
// 3.52
//         {top: 965, left: 2025, info: {price: ' грн', contacts: ''}},
// 3.51
//         {top: 965, left: 2065, info: {price: ' грн', contacts: ''}},
// 3.50
//         {top: 965, left: 2105, info: {price: ' грн', contacts: ''}},
// 3.49
//         {top: 965, left: 2145, info: {price: ' грн', contacts: ''}},
//
//
// 4 поверх - верх
// 4.01
//         {top: 1215, left: 265, info: {price: ' грн', contacts: ''}},
// 4.02
//         {top: 1215, left: 305, info: {price: ' грн', contacts: ''}},
// 4.03
//         {top: 1215, left: 345, info: {price: ' грн', contacts: ''}},
// 4.04
//         {top: 1215, left: 385, info: {price: ' грн', contacts: ''}},
// 4.05
//         {top: 1215, left: 425, info: {price: ' грн', contacts: ''}},
// 4.06
//         {top: 1215, left: 465, info: {price: ' грн', contacts: ''}},
// 4.07
//         {top: 1215, left: 505, info: {price: ' грн', contacts: ''}},
// 4.08
//         {top: 1215, left: 545, info: {price: ' грн', contacts: ''}},
// 4.09
//         {top: 1215, left: 585, info: {price: ' грн', contacts: ''}},
// 4.10
//         {top: 1215, left: 625, info: {price: ' грн', contacts: ''}},
// 4.11
//         {top: 1215, left: 665, info: {price: ' грн', contacts: ''}},
// 4.12
//         {top: 1215, left: 705, info: {price: ' грн', contacts: ''}},
// 4.13
//         {top: 1215, left: 745, info: {price: ' грн', contacts: ''}},
// 4.14
//         {top: 1215, left: 785, info: {price: ' грн', contacts: ''}},
// 4.15
//         {top: 1215, left: 825, info: {price: ' грн', contacts: ''}},
// 4.16
//         {top: 1215, left: 865, info: {price: ' грн', contacts: ''}},
// 4.17
//         {top: 1215, left: 905, info: {price: ' грн', contacts: ''}},
// 4.18
//         {top: 1215, left: 945, info: {price: ' грн', contacts: ''}},
// 4.19
//         {top: 1215, left: 985, info: {price: ' грн', contacts: ''}},
// 4.20
//         {top: 1215, left: 1025, info: {price: ' грн', contacts: ''}},
// 4.21
//         {top: 1215, left: 1065, info: {price: ' грн', contacts: ''}},
// 4.22
//         {top: 1215, left: 1105, info: {price: ' грн', contacts: ''}},
// 4.23
//         {top: 1215, left: 1145, info: {price: ' грн', contacts: ''}},
// 4.24
//         {top: 1215, left: 1185, info: {price: ' грн', contacts: ''}},
// 4.25
//         {top: 1215, left: 1225, info: {price: ' грн', contacts: ''}},
// 4.26
//         {top: 1215, left: 1265, info: {price: ' грн', contacts: ''}},
// 4.27
//         {top: 1215, left: 1305, info: {price: ' грн', contacts: ''}},
// 4.28
//         {top: 1215, left: 1345, info: {price: ' грн', contacts: ''}},
// 4.29
//         {top: 1215, left: 1385, info: {price: ' грн', contacts: ''}},
// 4.30
//         {top: 1215, left: 1425, info: {price: ' грн', contacts: ''}},
// 4.31
//         {top: 1215, left: 1465, info: {price: ' грн', contacts: ''}},
// 4.32
//         {top: 1215, left: 1505, info: {price: ' грн', contacts: ''}},
// 4.33
//         {top: 1215, left: 1545, info: {price: ' грн', contacts: ''}},
// 4.34
//         {top: 1215, left: 1585, info: {price: ' грн', contacts: ''}},
// 4.35
//         {top: 1215, left: 1625, info: {price: ' грн', contacts: ''}},
// 4.36
//         {top: 1215, left: 1665, info: {price: ' грн', contacts: ''}},
// 4.37
//         {top: 1215, left: 1705, info: {price: ' грн', contacts: ''}},
// 4.38
//         {top: 1215, left: 1745, info: {price: ' грн', contacts: ''}},
// 4.39
//         {top: 1215, left: 1785, info: {price: ' грн', contacts: ''}},
// 4.40
//         {top: 1215, left: 1825, info: {price: ' грн', contacts: ''}},
// 4.41
//         {top: 1215, left: 1865, info: {price: ' грн', contacts: ''}},
// 4.42
//         {top: 1215, left: 1905, info: {price: ' грн', contacts: ''}},
// 4.43
//         {top: 1215, left: 1945, info: {price: ' грн', contacts: ''}},
// 4.44
//         {top: 1215, left: 1985, info: {price: ' грн', contacts: ''}},
// 4.45
//         {top: 1215, left: 2025, info: {price: ' грн', contacts: ''}},
// 4.46
//         {top: 1215, left: 2065, info: {price: ' грн', contacts: ''}},
// 4.47
//         {top: 1215, left: 2105, info: {price: ' грн', contacts: ''}},
// 4.48
//         {top: 1215, left: 2145, info: {price: ' грн', contacts: ''}},
//
//
// 4 поверх - низ
// 4.92
//         {top: 1365, left: 265, info: {price: ' грн', contacts: ''}},
// 4.91
//         {top: 1365, left: 305, info: {price: ' грн', contacts: ''}},
// 4.90
//         {top: 1365, left: 345, info: {price: ' грн', contacts: ''}},
// 4.89
//         {top: 1365, left: 385, info: {price: ' грн', contacts: ''}},
// 4.88
//         {top: 1365, left: 425, info: {price: ' грн', contacts: ''}},
// 4.87
//         {top: 1365, left: 465, info: {price: ' грн', contacts: ''}},
// 4.86
//         {top: 1365, left: 505, info: {price: ' грн', contacts: ''}},
// 4.85
//         {top: 1365, left: 545, info: {price: ' грн', contacts: ''}},
// 4.84
//         {top: 1365, left: 585, info: {price: ' грн', contacts: ''}},
// 4.83
//         {top: 1365, left: 625, info: {price: ' грн', contacts: ''}},
// 4.82
//         {top: 1365, left: 665, info: {price: ' грн', contacts: ''}},
// 4.81
//         {top: 1365, left: 785, info: {price: ' грн', contacts: ''}},
// 4.80
//         {top: 1365, left: 825, info: {price: ' грн', contacts: ''}},
// 4.79
//         {top: 1365, left: 865, info: {price: ' грн', contacts: ''}},
// 4.78
//         {top: 1365, left: 905, info: {price: ' грн', contacts: ''}},
// 4.77
//         {top: 1365, left: 945, info: {price: ' грн', contacts: ''}},
// 4.76
//         {top: 1365, left: 985, info: {price: ' грн', contacts: ''}},
// 4.75
//         {top: 1365, left: 1025, info: {price: ' грн', contacts: ''}},
// 4.74
//         {top: 1365, left: 1065, info: {price: ' грн', contacts: ''}},
// 4.73
//         {top: 1365, left: 1105, info: {price: ' грн', contacts: ''}},
// 4.72
//         {top: 1365, left: 1145, info: {price: ' грн', contacts: ''}},
// 4.71
//         {top: 1365, left: 1185, info: {price: ' грн', contacts: ''}},
// 4.70
//         {top: 1365, left: 1225, info: {price: ' грн', contacts: ''}},
// 4.69
//         {top: 1365, left: 1265, info: {price: ' грн', contacts: ''}},
// 4.68
//         {top: 1365, left: 1305, info: {price: ' грн', contacts: ''}},
// 4.67
//         {top: 1365, left: 1345, info: {price: ' грн', contacts: ''}},
// 4.66
//         {top: 1365, left: 1385, info: {price: ' грн', contacts: ''}},
// 4.65
//         {top: 1365, left: 1425, info: {price: ' грн', contacts: ''}},
// 4.64
//         {top: 1365, left: 1465, info: {price: ' грн', contacts: ''}},
// 4.63
//         {top: 1365, left: 1505, info: {price: ' грн', contacts: ''}},
// 4.62
//         {top: 1365, left: 1545, info: {price: ' грн', contacts: ''}},
// 4.61
//         {top: 1365, left: 1585, info: {price: ' грн', contacts: ''}},
// 4.60
//         {top: 1365, left: 1625, info: {price: ' грн', contacts: ''}},
// 4.59
//         {top: 1365, left: 1745, info: {price: ' грн', contacts: ''}},
// 4.58
//         {top: 1365, left: 1785, info: {price: ' грн', contacts: ''}},
// 4.57
//         {top: 1365, left: 1825, info: {price: ' грн', contacts: ''}},
// 4.56
//         {top: 1365, left: 1865, info: {price: ' грн', contacts: ''}},
// 4.55
//         {top: 1365, left: 1905, info: {price: ' грн', contacts: ''}},
// 4.54
//         {top: 1365, left: 1945, info: {price: ' грн', contacts: ''}},
// 4.53
//         {top: 1365, left: 1985, info: {price: ' грн', contacts: ''}},
// 4.52
//         {top: 1365, left: 2025, info: {price: ' грн', contacts: ''}},
// 4.51
//         {top: 1365, left: 2065, info: {price: ' грн', contacts: ''}},
// 4.50
//         {top: 1365, left: 2105, info: {price: ' грн', contacts: ''}},
// 4.49
//         {top: 1365, left: 2145, info: {price: ' грн', contacts: ''}},
//
//
// 5 поверх - верх
// 5.01
//         {top: 1615, left: 265, info: {price: ' грн', contacts: ''}},
// 5.02
//         {top: 1615, left: 305, info: {price: ' грн', contacts: ''}},
// 5.03
//         {top: 1615, left: 345, info: {price: ' грн', contacts: ''}},
// 5.04
//         {top: 1615, left: 385, info: {price: ' грн', contacts: ''}},
// 5.05
//         {top: 1615, left: 425, info: {price: ' грн', contacts: ''}},
// 5.06
//         {top: 1615, left: 465, info: {price: ' грн', contacts: ''}},
// 5.07
//         {top: 1615, left: 505, info: {price: ' грн', contacts: ''}},
// 5.08
//         {top: 1615, left: 545, info: {price: ' грн', contacts: ''}},
// 5.09
//         {top: 1615, left: 585, info: {price: ' грн', contacts: ''}},
// 5.10
//         {top: 1615, left: 625, info: {price: ' грн', contacts: ''}},
// 5.11
//         {top: 1615, left: 665, info: {price: ' грн', contacts: ''}},
// 5.12
//         {top: 1615, left: 705, info: {price: ' грн', contacts: ''}},
// 5.13
//         {top: 1615, left: 745, info: {price: ' грн', contacts: ''}},
// 5.14
//         {top: 1615, left: 785, info: {price: ' грн', contacts: ''}},
// 5.15
//         {top: 1615, left: 825, info: {price: ' грн', contacts: ''}},
// 5.16
//         {top: 1615, left: 865, info: {price: ' грн', contacts: ''}},
// 5.17
//         {top: 1615, left: 905, info: {price: ' грн', contacts: ''}},
// 5.18
//         {top: 1615, left: 945, info: {price: ' грн', contacts: ''}},
// 5.19
//         {top: 1615, left: 985, info: {price: ' грн', contacts: ''}},
// 5.20
//         {top: 1615, left: 1025, info: {price: ' грн', contacts: ''}},
// 5.21
//         {top: 1615, left: 1065, info: {price: ' грн', contacts: ''}},
// 5.22
//         {top: 1615, left: 1105, info: {price: ' грн', contacts: ''}},
// 5.23
//         {top: 1615, left: 1145, info: {price: ' грн', contacts: ''}},
// 5.24
//         {top: 1615, left: 1185, info: {price: ' грн', contacts: ''}},
// 5.25
//         {top: 1615, left: 1225, info: {price: ' грн', contacts: ''}},
// 5.26
//         {top: 1615, left: 1265, info: {price: ' грн', contacts: ''}},
// 5.27
//         {top: 1615, left: 1305, info: {price: ' грн', contacts: ''}},
// 5.28
//         {top: 1615, left: 1345, info: {price: ' грн', contacts: ''}},
// 5.29
//         {top: 1615, left: 1385, info: {price: ' грн', contacts: ''}},
// 5.30
//         {top: 1615, left: 1425, info: {price: ' грн', contacts: ''}},
// 5.31
//         {top: 1615, left: 1465, info: {price: ' грн', contacts: ''}},
// 5.32
//         {top: 1615, left: 1505, info: {price: ' грн', contacts: ''}},
// 5.33
//         {top: 1615, left: 1545, info: {price: ' грн', contacts: ''}},
// 5.34
//         {top: 1615, left: 1585, info: {price: ' грн', contacts: ''}},
// 5.35
//         {top: 1615, left: 1625, info: {price: ' грн', contacts: ''}},
// 5.36
//         {top: 1615, left: 1665, info: {price: ' грн', contacts: ''}},
// 5.37
//         {top: 1615, left: 1705, info: {price: ' грн', contacts: ''}},
// 5.38
//         {top: 1615, left: 1745, info: {price: ' грн', contacts: ''}},
// 5.39
//         {top: 1615, left: 1785, info: {price: ' грн', contacts: ''}},
// 5.40
//         {top: 1615, left: 1825, info: {price: ' грн', contacts: ''}},
// 5.41
//         {top: 1615, left: 1865, info: {price: ' грн', contacts: ''}},
// 5.42
//         {top: 1615, left: 1905, info: {price: ' грн', contacts: ''}},
// 5.43
//         {top: 1615, left: 1945, info: {price: ' грн', contacts: ''}},
// 5.44
//         {top: 1615, left: 1985, info: {price: ' грн', contacts: ''}},
// 5.45
//         {top: 1615, left: 2025, info: {price: ' грн', contacts: ''}},
// 5.46
//         {top: 1615, left: 2065, info: {price: ' грн', contacts: ''}},
// 5.47
//         {top: 1615, left: 2105, info: {price: ' грн', contacts: ''}},
// 5.48
//         {top: 1615, left: 2145, info: {price: ' грн', contacts: ''}},
//
//
// 5 поверх - низ
// 5.92
//         {top: 1765, left: 265, info: {price: ' грн', contacts: ''}},
// 5.91
//         {top: 1765, left: 305, info: {price: ' грн', contacts: ''}},
// 5.90
//         {top: 1765, left: 345, info: {price: ' грн', contacts: ''}},
// 5.89
//         {top: 1765, left: 385, info: {price: ' грн', contacts: ''}},
// 5.88
//         {top: 1765, left: 425, info: {price: ' грн', contacts: ''}},
// 5.87
//         {top: 1765, left: 465, info: {price: ' грн', contacts: ''}},
// 5.86
//         {top: 1765, left: 505, info: {price: ' грн', contacts: ''}},
// 5.85
//         {top: 1765, left: 545, info: {price: ' грн', contacts: ''}},
// 5.84
//         {top: 1765, left: 585, info: {price: ' грн', contacts: ''}},
// 5.83
//         {top: 1765, left: 625, info: {price: ' грн', contacts: ''}},
// 5.82
//         {top: 1765, left: 665, info: {price: ' грн', contacts: ''}},
// 5.81
//         {top: 1765, left: 785, info: {price: ' грн', contacts: ''}},
// 5.80
//         {top: 1765, left: 825, info: {price: ' грн', contacts: ''}},
// 5.79
//         {top: 1765, left: 865, info: {price: ' грн', contacts: ''}},
// 5.78
//         {top: 1765, left: 905, info: {price: ' грн', contacts: ''}},
// 5.77
//         {top: 1765, left: 945, info: {price: ' грн', contacts: ''}},
// 5.76
//         {top: 1765, left: 985, info: {price: ' грн', contacts: ''}},
// 5.75
//         {top: 1765, left: 1025, info: {price: ' грн', contacts: ''}},
// 5.74
//         {top: 1765, left: 1065, info: {price: ' грн', contacts: ''}},
// 5.73
//         {top: 1765, left: 1105, info: {price: ' грн', contacts: ''}},
// 5.72
//         {top: 1765, left: 1145, info: {price: ' грн', contacts: ''}},
// 5.71
//         {top: 1765, left: 1185, info: {price: ' грн', contacts: ''}},
// 5.70
//         {top: 1765, left: 1225, info: {price: ' грн', contacts: ''}},
// 5.69
//         {top: 1765, left: 1265, info: {price: ' грн', contacts: ''}},
// 5.68
//         {top: 1765, left: 1305, info: {price: ' грн', contacts: ''}},
// 5.67
//         {top: 1765, left: 1345, info: {price: ' грн', contacts: ''}},
// 5.66
//         {top: 1765, left: 1385, info: {price: ' грн', contacts: ''}},
// 5.65
//         {top: 1765, left: 1425, info: {price: ' грн', contacts: ''}},
// 5.64
//         {top: 1765, left: 1465, info: {price: ' грн', contacts: ''}},
// 5.63
//         {top: 1765, left: 1505, info: {price: ' грн', contacts: ''}},
// 5.62
//         {top: 1765, left: 1545, info: {price: ' грн', contacts: ''}},
// 5.61
//         {top: 1765, left: 1585, info: {price: ' грн', contacts: ''}},
// 5.60
//         {top: 1765, left: 1625, info: {price: ' грн', contacts: ''}},
// 5.59
//         {top: 1765, left: 1745, info: {price: ' грн', contacts: ''}},
// 5.58
//         {top: 1765, left: 1785, info: {price: ' грн', contacts: ''}},
// 5.57
//         {top: 1765, left: 1825, info: {price: ' грн', contacts: ''}},
// 5.56
//         {top: 1765, left: 1865, info: {price: ' грн', contacts: ''}},
// 5.55
//         {top: 1765, left: 1905, info: {price: ' грн', contacts: ''}},
// 5.54
//         {top: 1765, left: 1945, info: {price: ' грн', contacts: ''}},
// 5.53
//         {top: 1765, left: 1985, info: {price: ' грн', contacts: ''}},
// 5.52
//         {top: 1765, left: 2025, info: {price: ' грн', contacts: ''}},
// 5.51
//         {top: 1765, left: 2065, info: {price: ' грн', contacts: ''}},
// 5.50
//         {top: 1765, left: 2105, info: {price: ' грн', contacts: ''}},
// 5.49
//         {top: 1765, left: 2145, info: {price: ' грн', contacts: ''}},
//
//
// 6 поверх - верх
// 6.01
//         {top: 2015, left: 265, info: {price: ' грн', contacts: ''}},
// 6.02
//         {top: 2015, left: 305, info: {price: ' грн', contacts: ''}},
// 6.03
//         {top: 2015, left: 345, info: {price: ' грн', contacts: ''}},
// 6.04
//         {top: 2015, left: 385, info: {price: ' грн', contacts: ''}},
// 6.05
//         {top: 2015, left: 425, info: {price: ' грн', contacts: ''}},
// 6.06
//         {top: 2015, left: 465, info: {price: ' грн', contacts: ''}},
// 6.07
//         {top: 2015, left: 505, info: {price: ' грн', contacts: ''}},
// 6.08
//         {top: 2015, left: 545, info: {price: ' грн', contacts: ''}},
// 6.09
//         {top: 2015, left: 585, info: {price: ' грн', contacts: ''}},
// 6.10
//         {top: 2015, left: 625, info: {price: ' грн', contacts: ''}},
// 6.11
//         {top: 2015, left: 665, info: {price: ' грн', contacts: ''}},
// 6.12
//         {top: 2015, left: 705, info: {price: ' грн', contacts: ''}},
// 6.13
//         {top: 2015, left: 745, info: {price: ' грн', contacts: ''}},
// 6.14
//         {top: 2015, left: 785, info: {price: ' грн', contacts: ''}},
// 6.15
//         {top: 2015, left: 825, info: {price: ' грн', contacts: ''}},
// 6.16
//         {top: 2015, left: 865, info: {price: ' грн', contacts: ''}},
// 6.17
//         {top: 2015, left: 905, info: {price: ' грн', contacts: ''}},
// 6.18
//         {top: 2015, left: 945, info: {price: ' грн', contacts: ''}},
// 6.19
//         {top: 2015, left: 985, info: {price: ' грн', contacts: ''}},
// 6.20
//         {top: 2015, left: 1025, info: {price: ' грн', contacts: ''}},
// 6.21
//         {top: 2015, left: 1065, info: {price: ' грн', contacts: ''}},
// 6.22
//         {top: 2015, left: 1105, info: {price: ' грн', contacts: ''}},
// 6.23
//         {top: 2015, left: 1145, info: {price: ' грн', contacts: ''}},
// 6.24
//         {top: 2015, left: 1185, info: {price: ' грн', contacts: ''}},
// 6.25
//         {top: 2015, left: 1225, info: {price: ' грн', contacts: ''}},
// 6.26
//         {top: 2015, left: 1265, info: {price: ' грн', contacts: ''}},
// 6.27
//         {top: 2015, left: 1305, info: {price: ' грн', contacts: ''}},
// 6.28
//         {top: 2015, left: 1345, info: {price: ' грн', contacts: ''}},
// 6.29
//         {top: 2015, left: 1385, info: {price: ' грн', contacts: ''}},
// 6.30
//         {top: 2015, left: 1425, info: {price: ' грн', contacts: ''}},
// 6.31
//         {top: 2015, left: 1465, info: {price: ' грн', contacts: ''}},
// 6.32
//         {top: 2015, left: 1505, info: {price: ' грн', contacts: ''}},
// 6.33
        {top: 2015, left: 1545, info: {price: '2500 грн + комуналка', contacts: '097-362-4632'}},
// 6.34
//         {top: 2015, left: 1585, info: {price: ' грн', contacts: ''}},
// 6.35
//         {top: 2015, left: 1625, info: {price: ' грн', contacts: ''}},
// 6.36
//         {top: 2015, left: 1665, info: {price: ' грн', contacts: ''}},
// 6.37
//         {top: 2015, left: 1705, info: {price: ' грн', contacts: ''}},
// 6.38
//         {top: 2015, left: 1745, info: {price: ' грн', contacts: ''}},
// 6.39
//         {top: 2015, left: 1785, info: {price: ' грн', contacts: ''}},
// 6.40
//         {top: 2015, left: 1825, info: {price: ' грн', contacts: ''}},
// 6.41
//         {top: 2015, left: 1865, info: {price: ' грн', contacts: ''}},
// 6.42
//         {top: 2015, left: 1905, info: {price: ' грн', contacts: ''}},
// 6.43
//         {top: 2015, left: 1945, info: {price: ' грн', contacts: ''}},
// 6.44
//         {top: 2015, left: 1985, info: {price: ' грн', contacts: ''}},
// 6.45
//         {top: 2015, left: 2025, info: {price: ' грн', contacts: ''}},
// 6.46
//         {top: 2015, left: 2065, info: {price: ' грн', contacts: ''}},
// 6.47
//         {top: 2015, left: 2105, info: {price: ' грн', contacts: ''}},
// 6.48
//         {top: 2015, left: 2145, info: {price: ' грн', contacts: ''}},
//
//
// 6 поверх - низ
// 6.92
//         {top: 2165, left: 265, info: {price: ' грн', contacts: ''}},
// 6.91
//         {top: 2165, left: 305, info: {price: ' грн', contacts: ''}},
// 6.90
//         {top: 2165, left: 345, info: {price: ' грн', contacts: ''}},
// 6.89
//         {top: 2165, left: 385, info: {price: ' грн', contacts: ''}},
// 6.88
//         {top: 2165, left: 425, info: {price: ' грн', contacts: ''}},
// 6.87
//         {top: 2165, left: 465, info: {price: ' грн', contacts: ''}},
// 6.86
//         {top: 2165, left: 505, info: {price: ' грн', contacts: ''}},
// 6.85
//         {top: 2165, left: 545, info: {price: ' грн', contacts: ''}},
// 6.84
//         {top: 2165, left: 585, info: {price: ' грн', contacts: ''}},
// 6.83
//         {top: 2165, left: 625, info: {price: ' грн', contacts: ''}},
// 6.82
//         {top: 2165, left: 665, info: {price: ' грн', contacts: ''}},
// 6.81
//         {top: 2165, left: 785, info: {price: ' грн', contacts: ''}},
// 6.80
//         {top: 2165, left: 825, info: {price: ' грн', contacts: ''}},
// 6.79
//         {top: 2165, left: 865, info: {price: ' грн', contacts: ''}},
// 6.78
//         {top: 2165, left: 905, info: {price: ' грн', contacts: ''}},
// 6.77
//         {top: 2165, left: 945, info: {price: ' грн', contacts: ''}},
// 6.76
//         {top: 2165, left: 985, info: {price: ' грн', contacts: ''}},
// 6.75
//         {top: 2165, left: 1025, info: {price: ' грн', contacts: ''}},
// 6.74
//         {top: 2165, left: 1065, info: {price: ' грн', contacts: ''}},
// 6.73
//         {top: 2165, left: 1105, info: {price: ' грн', contacts: ''}},
// 6.72
//         {top: 2165, left: 1145, info: {price: ' грн', contacts: ''}},
// 6.71
//         {top: 2165, left: 1185, info: {price: ' грн', contacts: ''}},
// 6.70
//         {top: 2165, left: 1225, info: {price: ' грн', contacts: ''}},
// 6.69
//         {top: 2165, left: 1265, info: {price: ' грн', contacts: ''}},
// 6.68
//         {top: 2165, left: 1305, info: {price: ' грн', contacts: ''}},
// 6.67
//         {top: 2165, left: 1345, info: {price: ' грн', contacts: ''}},
// 6.66
//         {top: 2165, left: 1385, info: {price: ' грн', contacts: ''}},
// 6.65
//         {top: 2165, left: 1425, info: {price: ' грн', contacts: ''}},
// 6.64
//         {top: 2165, left: 1465, info: {price: ' грн', contacts: ''}},
// 6.63
//         {top: 2165, left: 1505, info: {price: ' грн', contacts: ''}},
// 6.62
//         {top: 2165, left: 1545, info: {price: ' грн', contacts: ''}},
// 6.61
//         {top: 2165, left: 1585, info: {price: ' грн', contacts: ''}},
// 6.60
//         {top: 2165, left: 1625, info: {price: ' грн', contacts: ''}},
// 6.59
//         {top: 2165, left: 1745, info: {price: ' грн', contacts: ''}},
// 6.58
//         {top: 2165, left: 1785, info: {price: ' грн', contacts: ''}},
// 6.57
//         {top: 2165, left: 1825, info: {price: ' грн', contacts: ''}},
// 6.56
//         {top: 2165, left: 1865, info: {price: ' грн', contacts: ''}},
// 6.55
//         {top: 2165, left: 1905, info: {price: ' грн', contacts: ''}},
// 6.54
//         {top: 2165, left: 1945, info: {price: ' грн', contacts: ''}},
// 6.53
//         {top: 2165, left: 1985, info: {price: ' грн', contacts: ''}},
// 6.52
//         {top: 2165, left: 2025, info: {price: ' грн', contacts: ''}},
// 6.51
//         {top: 2165, left: 2065, info: {price: ' грн', contacts: ''}},
// 6.50
//         {top: 2165, left: 2105, info: {price: ' грн', contacts: ''}},
// 6.49
//         {top: 2165, left: 2145, info: {price: ' грн', contacts: ''}}

    ];

    // Додаємо іконки на сторінку
    iconCoordinates.forEach(function (coordinates) {
        var icon = document.createElement('div');
        icon.className = 'icon';
        icon.innerHTML = '&#9989;';
        icon.style.top = coordinates.top + 'px';
        icon.style.left = coordinates.left + 'px';
        iconsContainer.appendChild(icon);

        // Додаємо обробник кліку для кожної іконки
        icon.addEventListener('click', function () {
            // Показуємо попап
            popup.style.display = 'block';

            // Встановлюємо інформацію в попап
            price.textContent = 'Ціна за місяць: ' + coordinates.info.price;
            contacts.textContent = 'Контакти: ' + coordinates.info.contacts;

            // Встановлюємо позицію попапа над іконкою
            popup.style.top = (coordinates.top + 50) + 'px'; // Змінюйте потрібну відстань від іконки
            popup.style.left = (coordinates.left - 100) + 'px'; // Змінюйте потрібну відстань від іконки
        });
    });
});

// Функція для закриття попапа
function closePopup() {
    document.getElementById('popup').style.display = 'none';
}